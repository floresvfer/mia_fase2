#ifndef MIA_CHOWN_H
#define MIA_CHOWN_H

#include "structures.h"
#include "utils.h"

typedef struct chown {
    char *path;
    char *usr;
    bool recursivo;
    bool valid;
} Chown;

Chown *newChown(char *path, char *usr, bool recursivo, bool valid);

#endif //MIA_CHOWN_H
