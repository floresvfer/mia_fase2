#include "mkGrp.h"

mkGrp *newMkGrp(char *name, bool valid) {
    mkGrp *tmp = malloc(sizeof(mkGrp));

    tmp->name = newString(10);
    tmp->valid = valid;

    tmp->name = name;

    if (strlen(name) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -name OBLIGATORIO");
    }

    return tmp;
}
