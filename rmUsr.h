#ifndef RMUSR_H
#define RMUSR_H

#include "utils.h"

struct rmusr {
    char *usr;
    bool valid;
} typedef rmUsr;

rmUsr *newRmUsr(char *usr, bool valid);

#endif /* RMUSR_H */
