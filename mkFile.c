#include "mkFile.h"

mkFile *newMkFile(char *path, char *id, int size, char *cont, bool padre, bool valid) {
    mkFile *tmp = malloc(sizeof(mkFile));

    tmp->path = newString(PATH_LENGHT);
    tmp->cont = newString(PATH_LENGHT);
    tmp->id = newString(10);
    tmp->size = -1;

    tmp->path = path;
    tmp->cont = cont;
    tmp->id = id;
    tmp->size = size;
    tmp->valid = valid;
    tmp->padre = padre;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    return tmp;
}
