#include "edit.h"

Edit *newEdit(char *path, int size, char *cont, bool valid) {
    Edit *tmp = malloc(sizeof(Edit));

    tmp->path = newString(PATH_LENGHT);
    tmp->size = -1;
    tmp->cont = newString(PATH_LENGHT);
    tmp->valid = valid;

    tmp->path = path;
    tmp->cont = cont;
    tmp->size = size;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    if (strlen(cont) == 0 && size == -1) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -cont o -size OBLIGATORIO");
    }
    return tmp;
}
