#include "fDisk.h"

fDisk *newFDisk(int size, char unit, char *path, char type, char *fit, char *del, char *name, int add, bool valid) {

    fDisk *tmp = malloc(sizeof(fDisk));

    //inicializacion de variables
    tmp->size = -1;
    tmp->unit = newString(1);
    tmp->path = newString(PATH_LENGHT);
    tmp->type = newString(1);
    tmp->fit = newString(2);
    tmp->del = newString(4);
    tmp->name = newString(16);
    tmp->add = 0;
    tmp->valid = valid;

    //asignacion de variables
    tmp->size = size;

    if (unit == 'B') {
        tmp->unit = 'b';
    } else if (unit == 'K') {
        tmp->unit = 'k';
    } else if (unit == 'M') {
        tmp->unit = 'm';
    } else {
        tmp->unit = unit;
    }

    tmp->path = path;

    if (type == 'P') {
        tmp->type = 'p';
    } else if (type == 'E') {
        tmp->type = 'e';
    } else if (type == 'L') {
        tmp->type = 'l';
    } else {
        tmp->type = type;
    }

    if (stricmp(fit, "bf") == 0) {
        tmp->fit = "bf";
    } else if (stricmp(fit, "ff") == 0) {
        tmp->fit = "ff";
    } else if (stricmp(fit, "wf") == 0) {
        tmp->fit = "wf";
    } else {
        tmp->fit = fit;
    }

    tmp->fit = fit;
    tmp->del = del;
    tmp->name = name;
    tmp->add = add;

    //validacion de parametros OBLIGATORIOS
    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    if (strlen(name) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -name OBLIGATORIO");
    }

    if (strlen(del) == 0 && add == 0) {
        if (size == -1) {
            tmp->valid = FALSE;
            alert(WAR, "parametro -size OBLIGATORIO");
        }

        if (size < 0) {
            tmp->valid = FALSE;
            alert(WAR, "parametro -size MENOR A CERO");
        }
    }

    //validacion de parametros VALORES
    if (strlen(del) == 0) {
        if (unit != ' ') {
            if (tmp->unit != 'k' && tmp->unit != 'm' && tmp->unit != 'b') {
                tmp->valid = FALSE;
                alert(ALR, "valor -unit NO RECONOCIDO");
                tmp->unit = ' ';
            }
        } else {
            tmp->unit = 'k';
        }
    }

    if (strlen(del) == 0 && add == 0) {
        if (type != ' ') {
            if (tmp->type != 'p' && tmp->type != 'e' && tmp->type != 'l') {
                tmp->valid = FALSE;
                alert(ALR, "valor -type NO RECONOCIDO");
                tmp->type = ' ';
            }
        } else {
            tmp->type = 'p';
        }

        if (strlen(fit) > 0) {
            if (stricmp(fit, "bf") != 0 && stricmp(fit, "ff") != 0 && stricmp(fit, "wf")) {
                tmp->valid = FALSE;
                alert(ALR, "valor -fit NO RECONOCIDO");
                tmp->fit = newString(2);
            }
        } else {
            tmp->fit = "wf";
        }
    }

    if (strlen(del) > 0) {
        if (stricmp(del, "fast") != 0 && stricmp(del, "full")) {
            tmp->valid = FALSE;
            alert(ALR, "valor -delete NO RECONOCIDO");
            tmp->del = newString(4);
        }
    }

    return tmp;

}
