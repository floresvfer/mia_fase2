#include "mkFs.h"

mkFs *newMkFs(char *id, char *type, char *fs, bool valid) {
    mkFs *tmp = malloc(sizeof(mkFs));

    tmp->id = newString(10);
    tmp->type = newString(10);
    tmp->fs = newString(10);
    tmp->valid = valid;

    tmp->id = id;
    tmp->type = type;
    tmp->fs = fs;

    if (strlen(id) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -id OBLIGATORIO");
    }

    return tmp;
}
