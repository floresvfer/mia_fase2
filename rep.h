#ifndef REP_H_INCLUDED
#define REP_H_INCLUDED

#include "utils.h"

struct rep {
    char *name;
    char *path;
    char *id;
    char *ruta;
    bool valid;
} typedef Rep;

Rep *newRep(char *name, char *path, char *id, char *ruta, bool valid);

#endif // REP_H_INCLUDED
