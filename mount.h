#ifndef MOUNT_H_INCLUDED
#define MOUNT_H_INCLUDED

#include "utils.h"
#include <stdlib.h>

struct mount {
    char *path;
    char *name;
    bool valid;
} typedef mount;

mount *newMount(char *path, char *name, bool valid);

#endif // MOUNT_H_INCLUDED
