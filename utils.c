#include "utils.h"


void createContent(char *destiny, int bytes) {
    int bite = 0;
    for (int i = 0; i < bytes; i++) {
        if (bite == 10)
            bite = 0;

        sprintf(destiny, "%s%c", destiny, bite + '0');
        bite++;
    }
}

void alert(int type, char *message) {
    switch (type) {
        case 0:
            printf(RED "ERROR: " RESET);
            printf(WHT "%s\n\n" RESET, message);
            break;

        case 1:
            printf(GRN "SUCCESS: " RESET);
            printf(WHT "%s\n\n" RESET, message);
            break;

        case 2:
            printf(YEL "WARNING: " RESET);
            printf(WHT "%s\n" RESET, message);
            break;

        case 3:
            printf(BLU "COMMAND: " RESET);
            printf(WHT "%s\n" RESET, message);
            break;

        case 4:
            printf(CYN "ALERT: " RESET);
            printf(WHT "%s\n" RESET, message);
            break;

        default:
            break;
    }

}

void clearStr(char *str, int len) {
    int count = 0;
    while (count < len) {
        str[count] = '\0';
        count++;
    }
}

char *newString(int len) {
    char *tmp = malloc(sizeof(char) * len);
    clearStr(tmp, len);
    return tmp;
}

int stricmp(const char *p1, const char *p2) {
    register unsigned char *s1 = (unsigned char *) p1;
    register unsigned char *s2 = (unsigned char *) p2;
    unsigned char c1, c2;

    do {
        c1 = (unsigned char) toupper((int) *s1++);
        c2 = (unsigned char) toupper((int) *s2++);
        if (c1 == '\0') {
            return c1 - c2;
        }
    } while (c1 == c2);

    return c1 - c2;
}

bool isNumber(char *number) {
    int index = 0;
    while (index < strlen(number)) {

        if (!((int) number[index] >= 48 && (int) number[index] <= 57) && ((int) number[index] != 45))
            return FALSE;

        index++;
    }
    return TRUE;
}


bool isParameter(char *source) {
    return strstr(source, "->") != NULL;
}

bool getMbr(Mbr *mbr, char *filename) {
    FILE *file;
    file = fopen(filename, "r+b");
    if (!file) {
        alert(ERR, "No se pudo obtener el MBR -");
        return FALSE;
    }
    fread(mbr, sizeof(Mbr), 1, file);
    fclose(file);
    return TRUE;
}


Partition *getPartition(Partition *partition, char *path, char *name) {
    Mbr mbr;
    if (getMbr(&mbr, path)) {
        Partition *array[4];

        for (int i = 0; i < 4; i++) {
            array[i] = &(mbr.mbr_partition[i]);
        }

        Partition *(*p)[] = &array;
        bool found = FALSE;
        for (int i = 0; i < 4; i++) {
            Partition *partition = (*p)[i];
            if (strcmp(partition->part_name, name) == 0) {
                return partition;
            }
        }
    }
}

bool tienePermiso(char permiso, char usuario, int number) {
    int num = 1000;

    int num2 = num + number;

    char tmp[4];

    sprintf(tmp, "%d", num2);

    char val;
    switch (usuario) {
        case 'u':
            val = tmp[1];
            break;
        case 'g':
            val = tmp[2];
            break;
        case 'o':
            val = tmp[3];
            break;
    }

    switch (permiso) {
        case 'r':
            if (val == '4' || val == '5' || val == '6' || val == '7')
                return TRUE;
            break;
        case 'w':
            if (val == '2' || val == '3' || val == '6' || val == '7')
                return TRUE;
            break;
        case 'x':
            if (val == '1' || val == '3' || val == '5' || val == '7')
                return TRUE;
            break;
    }
    return FALSE;
}


int ceilgg(float num) {
    int inum = (int) num;
    if (num == (float) inum) {
        return inum;
    }
    return inum + 1;
}





/* r w x
0  0 0 0
1  0 0 1
2  0 1 0
3  0 1 1
4  1 0 0
5  1 0 1
6  1 1 0
7  1 1 1*/
