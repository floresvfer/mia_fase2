#ifndef MKFILE_H
#define MKFILE_H

#include "utils.h"

struct mkfile {
    char *path;
    char *id;
    int size;
    char *cont;
    bool padre;
    bool valid;
} typedef mkFile;

mkFile *newMkFile(char *path, char *id, int size, char *cont, bool padre, bool valid);

#endif /* MKFILE_H */
