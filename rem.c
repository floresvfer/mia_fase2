#include "rem.h"

Rem *newRem(char *path, bool valid) {
    Rem *tmp = malloc(sizeof(Rem));
    tmp->path = newString(PATH_LENGHT);
    tmp->valid = valid;
    tmp->path = path;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }
    return tmp;
}