#include "chown.h"

Chown *newChown(char *path, char *usr, bool recursivo, bool valid) {
    Chown *tmp = malloc(sizeof(Chown));
    tmp->path = newString(PATH_LENGHT);
    tmp->usr = newString(10);
    tmp->valid = valid;
    tmp->recursivo = recursivo;

    tmp->path = path;
    tmp->usr = usr;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    if (strlen(usr) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -usr OBLIGATORIO");
    }
    return tmp;
}
