#include "disk.h"

disk *create_disk(char *id, char *path, char *name, disk *next) {
    disk *newDisk = malloc(sizeof(disk));
    newDisk->path = newString(PATH_LENGHT);
    newDisk->name = newString(16);
    newDisk->id = newString(8);

    newDisk->path = path;
    newDisk->name = name;
    newDisk->id = id;
    newDisk->next = next;

    return newDisk;
}

node *create_node(char *path, char letra, int count, node *next) {
    node *newNode = malloc(sizeof(node));
    newNode->path = newString(PATH_LENGHT);

    newNode->path = path;
    newNode->letra = letra;
    newNode->count = count;
    newNode->next = next;

    return newNode;
}
