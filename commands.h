#ifndef COMMANDS_H_INCLUDED
#define COMMANDS_H_INCLUDED

#include "utils.h"

#include "mkDisk.h"
#include "rmDisk.h"
#include "fDisk.h"
#include "mount.h"
#include "unMount.h"
#include "list.h"
#include "binaryFiles.h"

extern int idUser;
extern int idGrupo;
extern char *diskName;


int getCommand(char *command);

int commandType(char *name);

int getSession();


void command_login(char *command, int index);

void command_mkdisk(char *command, int index);

void command_rmdisk(char *command, int index);

void command_fdisk(char *command, int index);

void command_mount(char *command, int index);

void command_unmount(char *command, int index);

void command_mkfs(char *command, int index);

void command_mkfile(char *command, int index);

void command_rem(char *command, int index);

void command_chmod(char *command, int index);

void command_chown(char *command, int index);

void command_mkgrp(char *command, int index);

void command_rmgrp(char *command, int index);

void command_mkusr(char *command, int index);

void command_rmusr(char *command, int index);

void command_edit(char *command, int index);

void command_cat(char *command, int index);

void command_ren(char *command, int index);

void command_cp(char *command, int index);

void command_mv(char *command, int index);

void command_mkdir(char *command, int index);

void command_exec(char *command, int index);

void command_rep(char *command, int index);

void command_logout(char *command, int index);

void command_recover(char *command, int index);

void command_loss(char *command, int index);

#endif // COMMANDS_H_INCLUDED
