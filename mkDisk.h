#ifndef MKDISK_H_INCLUDED
#define MKDISK_H_INCLUDED

#include "utils.h"

struct mkdisk {
    char *path;
    char *unit;
    int size;
    bool valid;
} typedef mkDisk;

mkDisk *newMkDisk(char *path, char *unit, int size, bool valid);

#endif // MKDISK_H_INCLUDED
