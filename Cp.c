#include "Cp.h"

Cp *newCp(char *path1, char *path2, bool valid) {
    Cp *tmp = malloc(sizeof(Cp));

    strcpy((char *) tmp->path1, path1);
    strcpy((char *) tmp->path2, path2);
    tmp->valid = valid;

    if (strlen(path1) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    if (strlen(path2) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -dest OBLIGATORIO");
    }

    return tmp;
}