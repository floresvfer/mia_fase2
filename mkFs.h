#ifndef MKFS_H
#define MKFS_H

#include "utils.h"

struct mkfs {
    char *id;
    char *type;
    char *fs;
    bool valid;
} typedef mkFs;

mkFs *newMkFs(char *id, char *type, char *fs, bool valid);

#endif

