#ifndef RMDISK_H_INCLUDED
#define RMDISK_H_INCLUDED

#include "utils.h"

struct rmdisk {
    char *path;
    bool valid;
} typedef rmDisk;

rmDisk *newRmDisk(char *path, bool valid);

#endif // RMDISK_H_INCLUDED
