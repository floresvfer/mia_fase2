#ifndef MIA_LOGIN_H
#define MIA_LOGIN_H

#include "structures.h"
#include "utils.h"

typedef struct login {
    char *usr;
    char *pwd;
    char *id;
    bool valid;
} Login;

Login *newLogIn(char *usr, char *pwd, char *id, bool valid);

#endif //MIA_LOGIN_H
