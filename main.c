/*mkDisk * a = newMkDisk("", 'b', -1);
printf("\n%s\n\n", "//////////////////////////////////////////////////////////////////////////////");
rmDisk * b = newRmDisk("");
printf("\n%s\n\n", "//////////////////////////////////////////////////////////////////////////////");
fDisk * c = newFDisk(0, 'c', "", 'z', "s", "s", "", 0);
printf("\n%s\n\n", "//////////////////////////////////////////////////////////////////////////////");
mount * d = newMount("", "");
printf("\n%s\n\n", "//////////////////////////////////////////////////////////////////////////////");
unMount * e = newUnMount("");*/


#include "binaryFiles.h"

#include "list.h"


extern list *mounts;
extern list_n *nodes;

extern int idUser;
extern int idGrupo;
extern char *diskName;


int main(int argc, char *argv[]) {

    initializeList();

    char line[256];

    while (stricmp(line, "exit") != 0) {
        for (int i = 0; i < strlen(line); i++)
            line[i] = '\0';

        if (fgets(line, sizeof(line), stdin)) {

            size_t ln = strlen(line) - 1;

            if (line[ln] == '\n')
                line[ln] = '\0';

            if (stricmp(line, "exit") != 0)
                getCommand(line);

        }
    }
    return 0;
}







