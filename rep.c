#include "rep.h"

Rep *newRep(char *name, char *path, char *id, char *ruta, bool valid) {
    Rep *tmp = malloc(sizeof(Rep));

    tmp->name = newString(10);
    tmp->path = newString(PATH_LENGHT);
    tmp->id = newString(8);
    tmp->ruta = newString(PATH_LENGHT);
    tmp->valid = valid;

    tmp->name = name;
    tmp->path = path;
    tmp->id = id;
    tmp->ruta = ruta;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    if (strlen(name) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -name OBLIGATORIO");
    } else {
        if (stricmp(name, "mbr") != 0 && stricmp(name, "disk") != 0 && stricmp(name, "inode") != 0 &&
            stricmp(name, "journaling") != 0 && stricmp(name, "block") != 0 &&
            stricmp(name, "bm_inode") != 0 && stricmp(name, "bm_block") != 0 && stricmp(name, "tree") != 0 &&
            stricmp(name, "sb") != 0 && stricmp(name, "file") != 0 &&
            stricmp(name, "ls") != 0) {
            tmp->valid = FALSE;
            alert(ALR, "valor -name NO RECONOCIDO");
            tmp->name = newString(16);
        }
    }

    if (strlen(id) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -id OBLIGATORIO");
    }
    return tmp;
}
