#ifndef MIA_MV_H
#define MIA_MV_H

#include "structures.h"
#include "utils.h"
typedef struct mv{
    char * path1 [PATH_LENGHT];
    char * path2 [PATH_LENGHT];
    bool valid;
}Mv;

Mv *newMv(char *path1, char *path2, bool valid);
#endif //MIA_MV_H
