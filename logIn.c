#include "logIn.h"

Login *newLogIn(char *usr, char *pwd, char *id, bool valid) {
    Login *tmp = malloc(sizeof(Login));
    tmp->usr = usr;
    tmp->pwd = pwd;
    tmp->id = id;
    tmp->valid = valid;

    if (strlen(usr) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -usr OBLIGATORIO");
    }

    if (strlen(pwd) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -pwd OBLIGATORIO");
    }

    if (strlen(id) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -id OBLIGATORIO");
    }
    return tmp;
}
