#ifndef MKGRP_H
#define MKGRP_H

#include "utils.h"

struct mkgrp {
    char *name;
    bool valid;
} typedef mkGrp;

mkGrp *newMkGrp(char *name, bool valid);

#endif /* MKGRP_H */
