#include "list.h"

extern char ids;

void insert_front(list *_list, char *id, char *path, char *name) {
    disk *new_node = create_disk(id, path, name, _list->head);
    _list->head = new_node;
}

void insert_front_n(list_n *_list, char *path, char letra, int count) {
    node *new_node = create_node(path, letra, count, _list->head);
    _list->head = new_node;
}


void insert_disk_top(list *_list, disk *_disk) {
    disk *cursor = _list->head;
    disk *prev = NULL;
    disk *new_node = create_disk(_disk->id, _disk->path, _disk->name, NULL);

    if (cursor == NULL) {
        _list->head = new_node;
    } else if (cursor->next == NULL) {
        if (strcmp(new_node->name, cursor->name) >= 0) {
            new_node->next = cursor;
            cursor->next = NULL;
        } else {
            cursor->next = new_node;
        }
    } else {
        while (cursor->next != NULL && strcmp(new_node->name, cursor->name) >= 0) {
            prev = cursor;
            cursor = cursor->next;
        }
        if (prev != NULL) {
            prev->next = new_node;
            new_node->next = cursor;
        }

    }

}

void insert_disk_top_n(list_n *_list, node *_disk) {
    node *cursor = _list->head;
    node *prev = NULL;
    node *new_node = create_node(_disk->path, _disk->letra, _disk->count, NULL);

    if (cursor == NULL) {
        _list->head = new_node;
    } else if (cursor->next == NULL) {
        if (strcmp(new_node->path, cursor->path) >= 0) {
            new_node->next = cursor;
            cursor->next = NULL;
        } else {
            cursor->next = new_node;
        }
    } else {
        while (cursor->next != NULL && strcmp(new_node->path, cursor->path) >= 0) {
            prev = cursor;
            cursor = cursor->next;
        }
        if (prev != NULL) {
            prev->next = new_node;
            new_node->next = cursor;
        }

    }

}


void delete_front(list *_list) {
    if (_list->head == NULL) {

    } else {
        disk *front = _list->head;
        _list->head = _list->head->next;
        front->next = NULL;
        /* is this the last node in the list */
        if (front == _list->head)
            _list->head = NULL;
        free(front);
    }
}

void delete_front_n(list_n *_list) {
    if (_list->head == NULL) {

    } else {
        node *front = _list->head;
        _list->head = _list->head->next;
        front->next = NULL;
        /* is this the last node in the list */
        if (front == _list->head)
            _list->head = NULL;
        free(front);
    }
}

//COLA
void insert_back(list *_list, char *id, char *path, char *name) {
    if (_list->head == NULL) {
        insert_front(_list, id, path, name);
    } else {
        /* go to the last node */
        disk *cursor = _list->head;
        while (cursor->next != NULL)
            cursor = cursor->next;

        /* create a new node */
        disk *new_node = create_disk(id, path, name, NULL);
        cursor->next = new_node;
    }
}

void insert_back_n(list_n *_list, char *path, char letra, int count) {
    if (_list->head == NULL) {
        insert_front_n(_list, path, letra, count);
    } else {
        /* go to the last node */
        node *cursor = _list->head;
        while (cursor->next != NULL)
            cursor = cursor->next;

        /* create a new node */
        node *new_node = create_node(path, letra, count, NULL);
        cursor->next = new_node;
    }
}


void delete_back(list *_list) {
    if (_list->head == NULL) {
        _list->head = NULL;
    } else {
        disk *cursor = _list->head;
        disk *back = NULL;
        while (cursor->next != NULL) {
            back = cursor;
            cursor = cursor->next;
        }

        if (back != NULL)
            back->next = NULL;

        /* if this is the last node in the list*/
        if (cursor == _list->head)
            _list->head = NULL;

        free(cursor);
    }
}

void delete_back_n(list_n *_list) {
    if (_list->head == NULL) {
        _list->head = NULL;
    } else {
        node *cursor = _list->head;
        node *back = NULL;
        while (cursor->next != NULL) {
            back = cursor;
            cursor = cursor->next;
        }

        if (back != NULL)
            back->next = NULL;

        /* if this is the last node in the list*/
        if (cursor == _list->head)
            _list->head = NULL;

        free(cursor);
    }
}


void remove_any(list *_list, char *id) {
    disk *nd = search(_list->head, _list->head, id);
    if (nd == NULL) {
        alert(ERR, "La particion que desea desmontar no se encuentra montada actualmente.");
    } else if (nd == _list->head) {
        delete_front(_list);
        alert(SCS, "Particion desmontada Exitosamente.");
    }
        /* if the node is the last node */
    else if (nd->next == NULL) {
        delete_back(_list);
        alert(SCS, "Particion desmontada Exitosamente.");
    }
        /* if the node is in the middle */
    else {
        disk *cursor = _list->head;
        while (cursor != NULL) {
            if (cursor->next == nd)
                break;
            cursor = cursor->next;
        }

        if (cursor != NULL) {
            disk *tmp = cursor->next;
            cursor->next = tmp->next;
            tmp->next = NULL;
            free(tmp);
        }
        alert(SCS, "Particion desmontada Exitosamente.");
    }
}


void remove_any_n(list_n *_list, char letra) {
    node *nd = search_n(_list->head, _list->head, letra);
    if (nd == NULL) {
    } else if (nd == _list->head) {
        delete_front_n(_list);
    }
        /* if the node is the last node */
    else if (nd->next == NULL) {
        delete_back_n(_list);
    }
        /* if the node is in the middle */
    else {
        node *cursor = _list->head;
        while (cursor != NULL) {
            if (cursor->next == nd)
                break;
            cursor = cursor->next;
        }

        if (cursor != NULL) {
            node *tmp = cursor->next;
            cursor->next = tmp->next;
            tmp->next = NULL;
            free(tmp);
        }
    }
}


disk *search(disk *head, disk *cursor, char *id) {//SEARCH FOR A VALUE (IT'S USED BY REMOVE FUNCTION
    if (cursor != NULL) {
        if (cursor->next != head) {
            if (strcmp(cursor->id, id) == 0) {
                return cursor;
            }
            search(head, cursor->next, id);

        } else {
            if (strcmp(cursor->id, id) == 0) {
                return cursor;
            } else {
                return NULL;
            }
        }
    } else {
        return NULL;
    }
}

disk *search_name(disk *head, disk *cursor, char *name, char *path) {//SEARCH FOR A VALUE (IT'S USED BY REMOVE FUNCTION
    if (cursor != NULL) {
        if (cursor->next != head) {
            if (strcmp(cursor->name, name) == 0 && strcmp(cursor->path, path) == 0) {
                return cursor;
            }
            search_name(head, cursor->next, name, path);

        } else {
            if (strcmp(cursor->name, name) == 0 && strcmp(cursor->path, path) == 0) {
                return cursor;
            } else {
                return NULL;
            }
        }
    } else {
        return NULL;
    }
}

node *search_n(node *head, node *cursor, char letra) {//SEARCH FOR A VALUE (IT'S USED BY REMOVE FUNCTION
    if (cursor != NULL) {
        if (cursor->next != head) {
            if (cursor->letra == letra) {
                return cursor;
            }
            search_n(head, cursor->next, letra);

        } else {
            if (cursor->letra == letra) {
                return cursor;
            } else {
                return NULL;
            }
        }
    } else {
        return NULL;
    }
}

node *search_n_path(node *head, node *cursor, char *path) {//SEARCH FOR A VALUE (IT'S USED BY REMOVE FUNCTION
    if (cursor != NULL) {
        if (cursor->next != head) {
            if (strcmp(cursor->path, path) == 0) {
                return cursor;
            }
            search_n_path(head, cursor->next, path);

        } else {
            if (strcmp(cursor->path, path) == 0) {
                return cursor;
            } else {
                return NULL;
            }
        }
    } else {
        return NULL;
    }
}


void dispose(list *_list) {
    disk *cursor, *tmp;

    if (_list->head != NULL) {
        cursor = _list->head;
        _list->head = NULL;
        while (cursor != NULL) {
            tmp = cursor->next;
            free(cursor);
            cursor = tmp;
        }
    }
}

void dispose_n(list_n *_list) {
    node *cursor, *tmp;

    if (_list->head != NULL) {
        cursor = _list->head;
        _list->head = NULL;
        while (cursor != NULL) {
            tmp = cursor->next;
            free(cursor);
            cursor = tmp;
        }
    }
}

void print(list *_list) {
    disk *cursor = _list->head;
    if (_list->head != NULL) {
        while (cursor != NULL) {
            printf(cursor->id);
            //printf("\n");
            //printf(cursor->path);
            //printf("\n");
            //printf(cursor->name);
            //printf("\n");
            if (cursor->next != NULL) {
                printf(" -> ");
            }
            cursor = cursor->next;
        }
    }
    printf("\n");
}

void print_n(list_n *_list) {
    node *cursor = _list->head;
    if (_list->head != NULL) {
        while (cursor != NULL) {
            printf("vd%c", cursor->letra);
            printf("%d", cursor->count);
            //printf("\n");
            //printf(cursor->path);
            //printf("\n");
            //printf(cursor->name);
            //printf("\n");
            if (cursor->next != NULL) {
                printf(" -> ");
            }
            cursor = cursor->next;
        }
    }
    printf("\n");
}
