#include "binaryFiles.h"

extern list *mounts;
extern list_n *nodes;

extern int idUser;
extern int idGrupo;
extern char *diskName;

extern char ids;

int getPartStart(char *id) {
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        Partition *part;
        part = getPartition((Partition *) &part, tmp->path, tmp->name);
        return part->part_start;
        /*printf("%d\n", part->part_size);
    printf("%s\n", part->part_name);*/
    } else {
        alert(ERR, "La particion Solicitada NO se encuentra MONTADA en el sistema");
    }
}

int getPartSize(char *id) {
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        Partition *part;
        part = getPartition((Partition *) &part, tmp->path, tmp->name);
        return part->part_size;
    } else {
        alert(ERR, "La particion Solicitada NO se encuentra MONTADA en el sistema");
    }
}

char *getPartName(char *id) {
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        Partition *part;
        part = getPartition((Partition *) &part, tmp->path, tmp->name);
        return part->part_name;
    } else {
        alert(ERR, "La particion Solicitada NO se encuentra MONTADA en el sistema");
    }
}

bool getSuperBlock(superBlock *sB, char *id) {
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);

        FILE *fp;

        fp = fopen(tmp->path, "r+b");
        if (!fp) {
            alert(ERR, "No se pudo obtener el super bloque.");
            return FALSE;
        }
        fseek(fp, part_start, SEEK_SET);
        fread(sB, sizeof(superBlock), 1, fp);
        fclose(fp);
    }
    return TRUE;
}

bool getInode(inode *In, char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int inodeTablePos = getInodeTablePos(sBpointer, index);


    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;

        fp = fopen(tmp->path, "r+b");
        if (!fp) {
            alert(ERR, "No se pudo accesar al disco.");
            return FALSE;
        }
        fseek(fp, inodeTablePos, SEEK_SET);
        fread(In, sizeof(inode), 1, fp);
        //fread(sB,sizeof(superBlock),1,fp);
        fclose(fp);
    }
    return TRUE;
}

bool getFileBlock(fBlock *fB, char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockTablePos = getBlockTablePos(sBpointer, index);
    //printf("%d\n", blockTablePos);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        if (!fp) {
            alert(ERR, "No se pudo accesar al disco.");
            return FALSE;
        }
        fseek(fp, blockTablePos, SEEK_SET);
        fread(fB, sizeof(fBlock), 1, fp);
        fclose(fp);
    }
    return TRUE;
}

bool getFolderBlock(dBlock *dB, char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockTablePos = getBlockTablePos(sBpointer, index);
    //printf("%d\n", blockTablePos);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        if (!fp) {
            alert(ERR, "No se pudo accesar al disco.");
            return FALSE;
        }
        fseek(fp, blockTablePos, SEEK_SET);
        fread(dB, sizeof(fBlock), 1, fp);
        fclose(fp);
    }
    return TRUE;
}

bool getPointerBlock(pBlock *pB, char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockTablePos = getBlockTablePos(sBpointer, index);
    //printf("%d\n", blockTablePos);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        if (!fp) {
            alert(ERR, "No se pudo accesar al disco.");
            return FALSE;
        }
        fseek(fp, blockTablePos, SEEK_SET);
        fread(pB, sizeof(fBlock), 1, fp);
        fclose(fp);
    }
    return TRUE;
}


void initializeList() {
    idGrupo = -1;
    idUser = -1;
    diskName = newString(16);
    mounts = malloc(sizeof(list));
    mounts->head = NULL;

    nodes = malloc(sizeof(list));
    nodes->head = NULL;

    ids = 'a';
}

bool newPartition(fDisk *data) {
    if (data->valid == TRUE) {

        if (data->size != -1) {
            if (strlen(data->del) == 0 && data->add == 0) {

                int bytes;
                if (data->unit == 'b') {
                    bytes = data->size;
                } else if (data->unit == 'k') {
                    bytes = 1024 * data->size;
                } else if (data->unit == 'm') {
                    bytes = 1024 * 1024 * data->size;
                }

                Mbr mbr;
                if (getMbr(&mbr, data->path)) {
                    Partition *array[4];
                    for (int i = 0; i < 4; i++) {
                        array[i] = &(mbr.mbr_partition[i]);
                    }
                    Partition *(*p)[] = &array;

                    /*printf(CYN "################### ACCESANDO AL DISCO ###################\n" RESET);
          printf("date of disk creation: %s\n",mbr.mbr_fecha_creacion);
          printf("amount of bytes: %d \n", mbr.mbr_tamanio);
          printf("amount of mbr's bytes: %lu \n", sizeof(Mbr));
          printf("amount of bytes to asign: %d \n\n\n", bytes);

          for (int i = 0; i < 4; i++)
          {
              Partition * partition = (*p)[i];
              printf("status: %c\n", partition->part_status);
              printf("type: %c\n", partition->part_type);
              printf("fit: %s\n", partition->part_fit);
              printf("start: %d\n", partition->part_start);
              printf("size: %d\n", partition->part_size);
              printf("name: %s\n", partition->part_name);
              printf("%s\n\n",".........................");
          }
          */
                    int used = 0;
                    bool oneExtended = FALSE;

                    for (int i = 0; i < 4; i++) {
                        Partition *partition = (*p)[i];

                        if (partition->part_status == PARTITION_STATUS_ACTIVE)
                            used++;

                        if (
                                partition->part_status == PARTITION_STATUS_ACTIVE &&
                                partition->part_type == 'e'
                                )
                            oneExtended = TRUE;
                    }

                    // limite de particiones
                    if (
                            data->type == 'e' ||
                            data->type == 'p'
                            ) {
                        if (used >= 4) {
                            alert(ERR, "Ya existen 4 particiones creadas en el Disco seleccionado");
                            return FALSE;
                        }
                    }

                    // solo una particion extendida
                    if (
                            data->type == 'e' &&
                            oneExtended
                            ) {
                        alert(ERR, "Ya existe una particion extendida en el Disco seleccionado");
                        return FALSE;
                    }

                    // extendida necesaria para logicas
                    if (
                            data->type == 'l' &&
                            !oneExtended
                            ) {
                        alert(ERR, "No existe una particion extendida en el Disco seleccionado");
                        return FALSE;
                    }

                    // VALIDACION DE NOMBRES
                    char nombre[16];
                    strncpy(nombre, data->name, 15);

                    for (int i = 0; i < 4; i++) {
                        Partition *partition = (*p)[i];

                        if (partition->part_status == PARTITION_STATUS_ACTIVE) {
                            if (strcmp(partition->part_name, data->name) == 0) {
                                alert(ERR, "Ya existe una particion con el mismo nombre en el Disco seleccionado");
                                return FALSE;
                            }
                        }
                    }

                    int no_particiones = 0;
                    int p_start = 0;
                    int p_size = bytes;
                    int free_space = 0;
                    bool procede = TRUE;

                    for (int i = 0; i < 4; i++) {
                        Partition *partition = (*p)[i];
                        if (partition->part_status == PARTITION_STATUS_ACTIVE) {
                            no_particiones++;
                        }
                    }

                    if (data->type == 'p' || data->type == 'e') {
                        if (no_particiones == 0) {
                            //printf("%s\n", "disco vacio");
                            free_space = getEspacioLibre(0, sizeof(Mbr), mbr.mbr_tamanio);
                            printf("%s%d\n", "espacio libre: ", free_space);
                            if (p_size <= free_space) {
                                p_start = sizeof(Mbr);
                                printf("\t%s %s\n",
                                       "Existe epacio suficiente en disco, se Procedera a crear la particion: ",
                                       data->name);
                                configPartition(&(mbr.mbr_partition[0]), PARTITION_STATUS_ACTIVE,
                                                data->type,
                                                data->fit,
                                                p_start,
                                                p_size,
                                                data->name);
                            } else {
                                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
                            }


                        } else if (no_particiones == 1) {
                            Partition *tmp = &(mbr.mbr_partition[0]);

                            free_space = sizeof(Mbr) - tmp->part_start;
                            if (p_size <= free_space) {
                                p_start = sizeof(Mbr);
                            } else {
                                free_space = mbr.mbr_tamanio - (tmp->part_start + tmp->part_size);
                                if (p_size <= free_space) {
                                    p_start = (tmp->part_start + tmp->part_size);
                                } else {
                                    procede = FALSE;
                                }
                            }


                            if (procede == TRUE) {
                                printf("%s%d\n", "espacio libre: ", free_space);
                                printf("\t%s %s\n",
                                       "Existe epacio suficiente en disco, se Procedera a crear la particion: ",
                                       data->name);
                                configPartition(&(mbr.mbr_partition[1]), PARTITION_STATUS_ACTIVE,
                                                data->type,
                                                data->fit,
                                                p_start,
                                                p_size,
                                                data->name);
                            } else {
                                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
                            }


                        } else if (no_particiones == 2) {
                            Partition *tmp = &(mbr.mbr_partition[0]);
                            Partition *tmp1 = &(mbr.mbr_partition[1]);

                            free_space = sizeof(Mbr) - tmp->part_start;
                            //printf("espacio libre mbr-primeraParticion: %d\n", free_space);
                            if (p_size <= free_space) {
                                p_start = sizeof(Mbr);
                            } else {
                                free_space = getEspacioLibre(tmp->part_start, tmp->part_size, tmp1->part_start);
                                //printf("espacio libre primeraParticion-segundaParticion: %d\n", free_space);
                                if (p_size <= free_space) {
                                    p_start = (tmp->part_start + tmp->part_size);
                                } else {
                                    free_space = mbr.mbr_tamanio - (tmp1->part_start + tmp1->part_size);
                                    //printf("espacio libre segundaParticion-finDisco: %d\n", free_space);
                                    if (p_size <= free_space) {
                                        p_start = (tmp1->part_start + tmp1->part_size);
                                    } else {
                                        procede = FALSE;
                                    }
                                }
                            }


                            if (procede == TRUE) {
                                printf("%s%d\n", "espacio libre: ", free_space);
                                printf("\t%s %s\n",
                                       "Existe epacio suficiente en disco, se Procedera a crear la particion: ",
                                       data->name);
                                configPartition(&(mbr.mbr_partition[2]), PARTITION_STATUS_ACTIVE,
                                                data->type,
                                                data->fit,
                                                p_start,
                                                p_size,
                                                data->name);
                            } else {
                                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
                            }

                        } else if (no_particiones == 3) {

                            Partition *tmp = &(mbr.mbr_partition[0]);
                            Partition *tmp1 = &(mbr.mbr_partition[1]);
                            Partition *tmp2 = &(mbr.mbr_partition[2]);

                            free_space = sizeof(Mbr) - tmp->part_start;
                            //printf("espacio libre mbr-primeraParticion: %d\n", free_space);
                            if (p_size <= free_space) {
                                p_start = sizeof(Mbr);
                            } else {
                                free_space = getEspacioLibre(tmp->part_start, tmp->part_size, tmp1->part_start);
                                //printf("espacio libre primeraParticion-segundaParticion: %d\n", free_space);
                                if (p_size <= free_space) {
                                    p_start = (tmp->part_start + tmp->part_size);
                                } else {
                                    free_space = getEspacioLibre(tmp1->part_start, tmp1->part_size, tmp2->part_start);
                                    //printf("espacio libre segundaParticion-terceraParticion: %d\n", free_space);
                                    if (p_size <= free_space) {
                                        p_start = (tmp1->part_start + tmp1->part_size);
                                    } else {
                                        free_space = mbr.mbr_tamanio - (tmp2->part_start + tmp2->part_size);
                                        //printf("espacio libre terceraParticion-finDisco: %d\n", free_space);
                                        if (p_size <= free_space) {
                                            p_start = (tmp2->part_start + tmp2->part_size);
                                        } else {
                                            procede = FALSE;
                                        }
                                    }
                                }
                            }

                            if (procede == TRUE) {
                                printf("%s%d\n", "espacio libre: ", free_space);
                                printf("\t%s %s\n",
                                       "Existe epacio suficiente en disco, se Procedera a crear la particion: ",
                                       data->name);
                                configPartition(&(mbr.mbr_partition[3]), PARTITION_STATUS_ACTIVE,
                                                data->type,
                                                data->fit,
                                                p_start,
                                                p_size,
                                                data->name);
                            } else {
                                alert(ERR, "NO existe ESPACIO SUFICIENTE en el disco para crear la particion");
                            }
                        }
                    } else {
                        procede = FALSE;
                    }
                    if (procede == TRUE) {
                        sortPartitions(&mbr);
                        FILE *f = fopen(data->path, "r+b");
                        rewind(f);
                        fwrite(&mbr, sizeof(Mbr), 1, f);
                        fclose(f);
                        alert(SCS, "Particion creada Exitosamente.");
                    }
                }
            }

        } else {
            if (data->size == -1 && strlen(data->del) != 0) {
                if (data->add == 0) {
                    if (data->type == ' ') {
                        if (strlen(data->fit) == 0) {
                            if (data->unit == ' ') {
                                Mbr mbr;
                                if (getMbr(&mbr, data->path)) {
                                    Partition *array[4];
                                    for (int i = 0; i < 4; i++) {
                                        array[i] = &(mbr.mbr_partition[i]);
                                    }
                                    Partition *(*p)[] = &array;
                                    printf(CYN "################### ACCESANDO AL DISCO ###################\n" RESET);
                                    printf("date of disk creation: %s\n", mbr.mbr_fecha_creacion);
                                    printf("amount of bytes: %d \n", mbr.mbr_tamanio);
                                    printf("amount of mbr's bytes: %lu \n", sizeof(Mbr));

                                    bool foud_it = FALSE;
                                    for (int i = 0; i < 4; i++) {
                                        Partition *partition = (*p)[i];
                                        if (partition->part_status == PARTITION_STATUS_ACTIVE) {
                                            if (strcmp(data->name, partition->part_name) == 0) {
                                                if (stricmp(data->del, "fast") == 0) {
                                                    configEmptyPartitionFast(&(mbr.mbr_partition[i]));
                                                } else if (stricmp(data->del, "full") == 0) {
                                                    configEmptyPartition(&(mbr.mbr_partition[i]));
                                                }
                                                i = 4;
                                                foud_it = TRUE;
                                            }
                                        }
                                    }
                                    if (foud_it == FALSE) {
                                        alert(ERR, "Particion NO ENCONTRADA en el disco.");
                                    } else {
                                        sortPartitions(&mbr);
                                        FILE *f = fopen(data->path, "r+b");
                                        rewind(f);
                                        fwrite(&mbr, sizeof(Mbr), 1, f);
                                        fclose(f);
                                        printf("\tParticion: %s encontrada exitosamente, se procedera a eliminarla.\n",
                                               data->name);
                                        alert(SCS, "Particion eliminada Exitosamente.");
                                    }
                                }
                            } else {
                                alert(WAR, "parametro -unit debe ser NULO");
                            }
                        } else {
                            alert(WAR, "parametro -fit debe ser NULO");
                        }
                    } else {
                        alert(WAR, "parametro -type debe ser NULO");
                    }
                } else {
                    alert(WAR, "parametro -add debe ser NULO");
                }
            } else {
                if (strlen(data->del) == 0 && data->add != 0) {


                    if (data->size == -1) {
                        if (data->type == ' ') {
                            if (strlen(data->fit) == 0) {

                                if (data->add > 0) {
                                    printf("añadir: %d\n", data->add);

                                    Mbr mbr;
                                    if (getMbr(&mbr, data->path)) {
                                        Partition *array[4];
                                        for (int i = 0; i < 4; i++) {
                                            array[i] = &(mbr.mbr_partition[i]);
                                        }
                                        Partition *(*p)[] = &array;
                                        printf(CYN "################### ACCESANDO AL DISCO ###################\n" RESET);
                                        printf("date of disk creation: %s\n", mbr.mbr_fecha_creacion);
                                        printf("amount of bytes: %d \n", mbr.mbr_tamanio);
                                        printf("amount of mbr's bytes: %lu \n", sizeof(Mbr));

                                        bool foud_it = FALSE;
                                        bool modified = FALSE;
                                        for (int i = 0; i < 4; i++) {
                                            Partition *partition = (*p)[i];
                                            if (partition->part_status == PARTITION_STATUS_ACTIVE) {
                                                if (strcmp(data->name, partition->part_name) == 0) {
                                                    int realsize = data->add;
                                                    if (data->unit == 'k') {
                                                        realsize = data->add * 1024;
                                                    } else if (data->unit == 'm') {
                                                        realsize = data->add * 1024 * 1024;
                                                    }


                                                    int espacioLibreAdelante = 0;
                                                    int espacioLibreAtras = 0;

                                                    if (i == 0) {

                                                        Partition *partition2 = (*p)[i + 1];

                                                        espacioLibreAtras = partition2->part_start -
                                                                            (partition->part_start +
                                                                             partition->part_size);
                                                        espacioLibreAdelante = partition->part_start - sizeof(Mbr);

                                                        if (espacioLibreAdelante >= realsize) {
                                                            configPartitionAddMinFront(&(mbr.mbr_partition[i]),
                                                                                       realsize);
                                                            modified = TRUE;
                                                        } else if (espacioLibreAtras >= realsize) {
                                                            configPartitionAddMinBack(&(mbr.mbr_partition[i]),
                                                                                      realsize);
                                                            modified = TRUE;
                                                        }

                                                    } else if (i == 1 || i == 2) {
                                                        Partition *partition1 = (*p)[i - 1];
                                                        Partition *partition2 = (*p)[i + 1];

                                                        espacioLibreAtras = partition2->part_start -
                                                                            (partition->part_start +
                                                                             partition->part_size);
                                                        espacioLibreAdelante = partition->part_start -
                                                                               (partition1->part_start +
                                                                                partition1->part_size);

                                                        if (espacioLibreAdelante >= realsize) {
                                                            configPartitionAddMinFront(&(mbr.mbr_partition[i]),
                                                                                       realsize);
                                                            modified = TRUE;
                                                        } else if (espacioLibreAtras >= realsize) {
                                                            configPartitionAddMinBack(&(mbr.mbr_partition[i]),
                                                                                      realsize);
                                                            modified = TRUE;
                                                        }

                                                    } else if (i == 3) {
                                                        Partition *partition1 = (*p)[i - 1];

                                                        espacioLibreAtras = mbr.mbr_tamanio - (partition->part_start +
                                                                                               partition->part_size);
                                                        espacioLibreAdelante = partition->part_start -
                                                                               (partition1->part_start +
                                                                                partition1->part_size);

                                                        if (espacioLibreAdelante >= realsize) {
                                                            configPartitionAddMinFront(&(mbr.mbr_partition[i]),
                                                                                       realsize);
                                                            modified = TRUE;
                                                        } else if (espacioLibreAtras >= realsize) {
                                                            configPartitionAddMinBack(&(mbr.mbr_partition[i]),
                                                                                      realsize);
                                                            modified = TRUE;
                                                        }
                                                    }

                                                    i = 4;
                                                    foud_it = TRUE;
                                                }
                                            }
                                        }
                                        if (foud_it == FALSE) {
                                            alert(ERR, "Particion NO ENCONTRADA en el disco.");
                                        } else {
                                            FILE *f = fopen(data->path, "r+b");
                                            rewind(f);
                                            fwrite(&mbr, sizeof(Mbr), 1, f);
                                            fclose(f);
                                            printf("\tParticion: %s encontrada exitosamente.\n", data->name);
                                            //espacios libres
                                            if (modified == TRUE) {
                                                alert(SCS, "Particion modificada Exitosamente.");
                                            } else {
                                                alert(ERR,
                                                      "No existe espacio suficiente para añadir el tamaño deseado.");
                                            }
                                        }
                                    }

                                } else {
                                    printf("restar: %d\n", data->add);

                                    Mbr mbr;
                                    if (getMbr(&mbr, data->path)) {
                                        Partition *array[4];
                                        for (int i = 0; i < 4; i++) {
                                            array[i] = &(mbr.mbr_partition[i]);
                                        }
                                        Partition *(*p)[] = &array;
                                        printf(CYN "################### ACCESANDO AL DISCO ###################\n" RESET);
                                        printf("date of disk creation: %s\n", mbr.mbr_fecha_creacion);
                                        printf("amount of bytes: %d \n", mbr.mbr_tamanio);
                                        printf("amount of mbr's bytes: %lu \n", sizeof(Mbr));

                                        bool foud_it = FALSE;
                                        for (int i = 0; i < 4; i++) {
                                            Partition *partition = (*p)[i];
                                            if (partition->part_status == PARTITION_STATUS_ACTIVE) {
                                                if (strcmp(data->name, partition->part_name) == 0) {
                                                    int realsize = data->add;
                                                    if (data->unit == 'k') {
                                                        realsize = data->add * 1024;
                                                    } else if (data->unit == 'm') {
                                                        realsize = data->add * 1024 * 1024;
                                                    }
                                                    configPartitionAddMinBack(&(mbr.mbr_partition[i]), realsize);
                                                    i = 4;
                                                    foud_it = TRUE;
                                                }
                                            }
                                        }
                                        if (foud_it == FALSE) {
                                            alert(ERR, "Particion NO ENCONTRADA en el disco.");
                                        } else {
                                            FILE *f = fopen(data->path, "r+b");
                                            rewind(f);
                                            fwrite(&mbr, sizeof(Mbr), 1, f);
                                            fclose(f);
                                            printf("\tParticion: %s encontrada exitosamente, se procedera a restar el espacio indicado.\n",
                                                   data->name);
                                            alert(SCS, "Particion modificada Exitosamente.");
                                        }
                                    }

                                }


                            } else {
                                alert(WAR, "parametro -fit debe ser NULO");
                            }
                        } else {
                            alert(WAR, "parametro -type debe ser NULO");
                        }
                    } else {
                        alert(WAR, "parametro -size debe ser NULO");
                    }
                }
            }
        }
    }
    free(data);
}

int getEspacioLibre(int ps1, int psz1, int ps2) {
    int inicio_libre = ps1 + psz1;
    int fin_libre = ps2;
    return fin_libre - inicio_libre;
}

void ingresar(Login *data) {
    char *destino = newString(280320);
    char *path = "/users.txt";
    char *name = newString(12);

    int folder = getFolder(data->id, path, TRUE);
    getFileFolderName(name, path);
    getFileContent(data->id, folder, name, destino);
    strcpy(diskName, data->id);
    logIn(destino, data->usr, data->pwd);
}

void newDisk(mkDisk *data) {
    char *local_file = data->path;

    char *ts1 = strdup(local_file);
    char *dir = dirname(ts1);

    if (data->valid == TRUE) {
        char *mkDir = newString(100);
        sprintf(mkDir, "mkdir -p \"%s\"/", dir);
        system(mkDir);
        free(mkDir);

        FILE *f = fopen(data->path, "wb");
        if (f != NULL) {

            /*int bytes;
      if(data->unit == 'k'){
          bytes = 1024*data->size;
      }else if(data->unit == 'm'){
          bytes = 1024*1024*data->size;
      }

      unsigned char byte[bytes];

      for (int i = 0; i < bytes; i++)
      {
          byte[i] = 0;
      }*/

            int bytes = 0;
            unsigned char byte[1024];
            unsigned char bytembr[(int) sizeof(Mbr)];

            for (int i = 0; i < 1024; i++) {
                byte[i] = 0;
            }

            for (int i = 0; i < 136; i++) {
                bytembr[i] = 0;
            }

            if (data->unit == (char *) 'k') {
                bytes = 1024 * data->size + ((int) sizeof(Mbr));
                for (int i = 0; i < data->size; i++) {
                    fwrite(byte, sizeof(byte), 1, f);
                }
            } else if (data->unit == (char *) 'm') {
                bytes = 1024 * 1024 * data->size + ((int) sizeof(Mbr));
                for (int i = 0; i < (1024 * data->size); i++) {
                    fwrite(byte, sizeof(byte), 1, f);
                }
            }

            fwrite(byte, sizeof(bytembr), 1, f);
            rewind(f);

            Mbr mbr;

            mbr.mbr_tamanio = bytes;
            getDate(mbr.mbr_fecha_creacion);

            mbr.mbr_disk_signature = rand();

            for (int i = 0; i < 4; i++) {
                configEmptyPartition(&(mbr.mbr_partition[i]));
                /*configPartition(&(mbr.mbr_partition[i]), PARTITION_STATUS_ACTIVE,
                                                 'p',
                                                 "bf",
                                                  i,
                                                 (i+1)*20,
                                                 "part");*/
            }

            sortPartitions(&mbr);

            fwrite(&mbr, sizeof(Mbr), 1, f);
            fclose(f);
            alert(SCS, "Disco creado Exitosamente.");
        }
    }
    free(data);
}

void delDisk(rmDisk *data) {
    if (data->valid == TRUE) {
        if (remove(data->path) == 0) {
            alert(SCS, "Disco eliminado Exitosamente.");
        } else {
            alert(ERR, "Disco inexistente.");
        }
    }
    free(data);
}

void genRep(Rep *data) {
    if (data->valid == TRUE) {
        //data->name
        //data->path
        //data->id
        Mbr mbr;
        Mbr *pointer;
        node *info;
        info = search_n(nodes->head, nodes->head, data->id[2]);

        if (info != NULL) {
            if (getMbr(&mbr, info->path)) {
                pointer = &mbr;
                if (stricmp(data->name, "mbr") == 0) {
                    graphMbr(pointer, data->path);
                    alert(SCS, "Reporte \"mbr\" creado Exitosamente.");
                } else if (stricmp(data->name, "disk") == 0) {
                    graphDisk(pointer, data->path);
                    alert(SCS, "Reporte \"disk\" creado Exitosamente.");
                } else if (stricmp(data->name, "bm_inode") == 0) {
                    printInodeBitMap(data->id, 20, '\t', data->path);
                } else if (stricmp(data->name, "bm_block") == 0) {
                    printBlockBitMap(data->id, 20, '\t', data->path);
                } else if (stricmp(data->name, "tree") == 0) {
                    graphDiskContent(data->id, data->path);
                } else if (stricmp(data->name, "inode") == 0) {
                    graphInodes(data->id, data->path);
                } else if (stricmp(data->name, "sb") == 0) {
                    superBlock Sb;
                    getSuperBlock(&Sb, data->id);
                    grapbSB(&Sb, data->path);
                } else if(stricmp(data->name, "file") == 0){
                    graphFileContent(data->id, data->path, data->ruta);
                }
            }
        } else {
            alert(ERR, "No se puede obtener informacion de la particion solicitada");
        }

    }
    free(data);
}

void mountPartition(mount *data) {
    if (data->valid == TRUE) {
        Mbr mbr;
        if (getMbr(&mbr, data->path)) {

            Partition *array[4];

            for (int i = 0; i < 4; i++) {

                array[i] = &(mbr.mbr_partition[i]);
            }
            Partition *(*p)[] = &array;
            bool found = FALSE;
            for (int i = 0; i < 4; i++) {
                Partition *partition = (*p)[i];

                if (strcmp(partition->part_name, data->name) == 0) {
                    printf("%s\n", partition->part_name);


                    if (search_name(mounts->head, mounts->head, data->name, data->path) == NULL) {

                        if (search_n_path(nodes->head, nodes->head, data->path) != NULL) {
                            search_n_path(nodes->head, nodes->head, data->path)->count++;
                        } else {
                            insert_front_n(nodes, data->path, ids, 0);
                            ids++;
                        }

                        node *info = search_n_path(nodes->head, nodes->head, data->path);
                        char *ID = newString(8);
                        sprintf(ID, "vd%c%d", info->letra, info->count);

                        insert_front(mounts, ID, data->path, data->name);

                        alert(SCS, "Particion montada Exitosamente.");
                    } else {
                        alert(WAR, "La particion especificada ya se encuentra montada actualemente.");
                    }
                    printf(CYN "############## PARTICIONES MONTADAS ACTUALMENTE ##############\n" RESET);
                    print(mounts);
                    printf("\n\n");
                    found = TRUE;
                }
            }
            if (!found) {
                alert(WAR, "Particion NO ENCONTRADA, no se puede montar la particion especificada.");
                printf("%s\n", "");
            }
        }

    }

    free(data);
}

void unMountPartition(unMount *data) {
    if (data->valid == TRUE) {
        remove_any(mounts, data->id);
        if (mounts->head != NULL) {
            printf(CYN "############## PARTICIONES MONTADAS ACTUALMENTE ##############\n" RESET);
            print(mounts);
        }
        printf("\n\n");
    }
    free(data);
}

void clearBits(char *id) {
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);
        superBlock sB;
        configSuperBlock(part_size, part_start, &sB, 3);

        FILE *fp;

        fp = fopen(tmp->path, "r+b");
        fseek(fp, part_start, SEEK_SET);
        fwrite(&sB, sizeof(superBlock), 1, fp);
        fclose(fp);
        writeBlockBitMap(id);
        writeInodeBitMap(id);

        int freeInode = firstFreeInode(id);
        int freeBlock = firstFreeBlock(id);

        //writeInode(id, freeInode, 1, 1, 0, '0', 755);
        writeInode(id, freeInode, 1, 1, 0, '0', 664);
        updateInodeAD(id, freeInode, 0, freeBlock);
        writeFirstFolderBlock(id, freeBlock, 0, 0);

        char *path;
        char *name;
        int folder = 0;

        path = "/users.txt";
        name = newString(12);
        char *cont = "1,G,root      \n1,U,root      ,root      ,123       \n";

        folder = getFolder(id, path, TRUE);
        getFileFolderName(name, path);
        writeFile(id, folder, name, cont);
        free(name);


        char *destino = newString(280320);
        path = "/users.txt";
        name = newString(12);

        folder = getFolder(id, path, TRUE);
        getFileFolderName(name, path);
        getFileContent(id, folder, name, destino);
        //printf("contenido : \n%s\n", destino);


        free(name);
        free(destino);

    } else {
        alert(ERR, "La particion Solicitada NO se encuentra MONTADA en el sistema");
    }
}


void mkFsPartition(mkFs *data) {
    if (data->valid == TRUE) {
        char *id = data->id;
        disk *tmp = search(mounts->head, mounts->head, id);
        if (tmp != NULL) {
            int part_size = getPartSize(id);
            int part_start = getPartStart(id);
            char *part_name = getPartName(id);
            superBlock sB;
            configSuperBlock(part_size, part_start, &sB, 3);

            FILE *fp;

            fp = fopen(tmp->path, "r+b");
            fseek(fp, part_start, SEEK_SET);
            fwrite(&sB, sizeof(superBlock), 1, fp);
            fclose(fp);
            writeBlockBitMap(id);
            writeInodeBitMap(id);
            writeJournal(id);

            int freeInode = firstFreeInode(id);
            int freeBlock = firstFreeBlock(id);

            //writeInode(id, freeInode, 1, 1, 0, '0', 755);
            writeInode(id, freeInode, 1, 1, 0, '0', 664);
            updateInodeAD(id, freeInode, 0, freeBlock);
            writeFirstFolderBlock(id, freeBlock, 0, 0);

            char *path;
            char *name;
            int folder = 0;

            path = "/users.txt";
            name = newString(12);
            char *cont = "1,G,root      \n1,U,root      ,root      ,123       \n";

            folder = getFolder(id, path, TRUE);
            getFileFolderName(name, path);
            writeFile(id, folder, name, cont);
            free(name);


            char *destino = newString(280320);
            path = "/users.txt";
            name = newString(12);

            folder = getFolder(id, path, TRUE);
            getFileFolderName(name, path);
            getFileContent(id, folder, name, destino);
            //printf("contenido : \n%s\n", destino);


            free(name);
            free(destino);



/*
      path = "/home/flores08/class/bar";
      //path = "/home";
      name = newString(12);

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFolder(id, folder, name);
      free(name);


      path = "/home/flores08/documents";
      //path = "/home";
      name = newString(12);

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFolder(id, folder, name);
      free(name);


      path = "/home/flores08/backup";
      //path = "/home";
      name = newString(12);

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFolder(id, folder, name);
      free(name);


      path = "/home/flores08/documents/files";
      //path = "/home";
      name = newString(12);

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFolder(id, folder, name);
      free(name);

      path = "/home/flores08/documents/doc.txt";
      name = newString(12);
      cont = "123456789012345678901234567890";

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFile(id, folder, name, cont);
      free(name);

      path = "/home/flores08/documents/files/doc1.txt";
      name = newString(12);
      cont = "123456789012345678901234567890";

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFile(id, folder, name, cont);
      free(name);

      path = "/home/flores08/documents/files/tmp";
      //path = "/home";
      name = newString(12);

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFolder(id, folder, name);
      free(name);

      path = "/home/flores08/documents/tmp2";
      //path = "/home";
      name = newString(12);

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFolder(id, folder, name);
      free(name);

      path = "/home/flores08/documents/files/tmp/tmp.txt";
      name = newString(12);
      cont = "123456789012345678901234567890";

      folder = getFolder(id, path, TRUE);
      getFileFolderName(name, path);
      writeFile(id, folder, name, cont);
      free(name);

*/

        } else {
            alert(ERR, "La particion Solicitada NO se encuentra MONTADA en el sistema");
        }
    }
    alert(SCS, "Formateo finalizado con exito.");
    free(data);
}

void makeFile(mkFile *data) {

    if (data->valid == TRUE) {
        char *path = "";
        char *name = newString(12);
        int folder = 0;
        char *cont = newString(280320);

        path = data->path;
        name = newString(12);


        if (strlen(data->cont) > 0) {
            FILE *fp = fopen(data->cont, "r");
            if (!fp) {
                printf("%s\n", "no se pudo obtener el archivo");
            } else {
                char ch;
                while ((ch = (char) fgetc(fp)) != EOF)
                    sprintf(cont, "%s%c", cont, ch);
            }
        } else {
            createContent(cont, data->size);
        }


        folder = getFolder(diskName, path, data->padre);
        getFileFolderName(name, path);
        writeFile(diskName, folder, name, cont);
        //addJournal(diskName, CREATE, ARCHIVO, cont, data->path, "", idUser, 644);
        addJournal(diskName, CREATE, ARCHIVO, cont, data->path, "", idUser, 664);
        free(name);
        free(cont);
    }
}

void removeFile_Folder(Rem *data) {
    if (data->valid == TRUE) {
        char *name = newString(12);
        int folder = getFolder(diskName, data->path, FALSE);
        getFileFolderName(name, data->path);
        removeFile(diskName, folder, name);
        free(name);
        addJournal(diskName, DELETE, 0, "", data->path, "", 0, 0);
    }
}

void chMod(Chmod *data) {
    if (data->valid == TRUE) {
        changePermisos(diskName, data->path, data->ugo, data->recursivo);
    }
}

void chOwn(Chown *data) {
    if (data->valid == TRUE) {
        char *file = newString(280320);
        char *path = "/users.txt";
        char *name = newString(12);

        int folder = getFolder(diskName, path, TRUE);
        getFileFolderName(name, path);
        getFileContent(diskName, folder, name, file);


        char *users = newString(300000);
        getAllUsers(file, users);
        int userID = findUserID(users, data->usr);
        if (userID > 0) {
            char *users = newString(300000);
            getAllUsers(file, users);
            char *grupo = newString(10);
            findUserGroup(users, data->usr, grupo);
            int groupID = findGroupID(file, grupo);
            free(grupo);

            changePropietario(diskName, data->path, userID, groupID, data->recursivo);
        } else {
            alert(ERR, "No existe el usuario especificado");
        }
    }
}


void makeGroup(mkGrp *data) {
    if (data->valid == TRUE) {
        if (idUser == 1) {
            char *name = newString(10);

            char *prev = newString(280320);
            char *new = newString(280320);

            char *path = "";
            int folder = 0;

            path = "/users.txt";

            folder = getFolder(diskName, path, FALSE);
            getFileFolderName(name, path);
            getFileContent(diskName, folder, name, prev);
            //printf("contenido : \n%s\n", destino);

            addGroup(prev, data->name, new);
            editFile(diskName, folder, name, new);


            free(name);
            free(prev);
            free(new);
        } else {
            alert(ERR, "Accion no permitida, solo usuario root");
        }
    }
}


void remGroup(mkGrp *data) {
    if (data->valid == TRUE) {
        if (idUser == 1) {
            char *name = newString(10);

            char *prev = newString(280320);
            char *new = newString(280320);

            char *path = "";
            int folder = 0;

            path = "/users.txt";

            folder = getFolder(diskName, path, FALSE);
            getFileFolderName(name, path);
            getFileContent(diskName, folder, name, prev);
            //printf("contenido : \n%s\n", destino);

            if (removeGroup(prev, data->name, new)) {
                editFile(diskName, folder, name, new);
            } else {
                alert(WAR, "NO se pudo ENCONTRAR el GRUPO especificado");
                alert(ERR, "NO se pudo ELIMINAR el GRUPO especificado");
            }

            free(name);
            free(prev);
            free(new);
        } else {
            alert(ERR, "Accion no permitida, solo usuario root");
        }
    }
}

void makeUser(mkUsr *data) {
    if (data->valid == TRUE) {
        if (idUser == 1) {
            char *name = newString(10);

            char *prev = newString(280320);
            char *new = newString(280320);

            char *path = "";
            int folder = 0;

            path = "/users.txt";

            folder = getFolder(diskName, path, FALSE);
            getFileFolderName(name, path);
            getFileContent(diskName, folder, name, prev);
            //printf("contenido : \n%s\n", destino);

            addUser(prev, data->grp, data->usr, data->pwd, new);
            editFile(diskName, folder, name, new);


            free(name);
            free(prev);
            free(new);
        } else {
            alert(ERR, "Accion no permitida, solo usuario root");
        }
    }
}

void remUser(rmUsr *data) {
    if (data->valid == TRUE) {
        if (idUser == 1) {
            char *name = newString(10);

            char *prev = newString(280320);
            char *new = newString(280320);

            char *path = "";
            int folder = 0;

            path = "/users.txt";

            folder = getFolder(diskName, path, FALSE);
            getFileFolderName(name, path);
            getFileContent(diskName, folder, name, prev);
            //printf("contenido : \n%s\n", destino);

            removeUser(prev, data->usr, new);
            editFile(diskName, folder, name, new);


            free(name);
            free(prev);
            free(new);
        } else {
            alert(ERR, "Accion no permitida, solo usuario root");
        }
    }
}

void edtFile(Edit *data) {

    if (data->valid == TRUE) {
        char *path = "";
        char *name = newString(12);
        int folder = 0;
        char *cont = newString(280320);

        path = data->path;
        name = newString(12);


        if (strlen(data->cont) > 0) {
            FILE *fp = fopen(data->cont, "r");
            if (!fp) {
                printf("%s\n", "no se pudo obtener el archivo");
            } else {
                char ch;
                while ((ch = (char) fgetc(fp)) != EOF)
                    sprintf(cont, "%s%c", cont, ch);
            }
        } else {
            createContent(cont, data->size);
        }

        folder = getFolder(diskName, path, FALSE);
        getFileFolderName(name, path);
        editFile(diskName, folder, name, cont);

        addJournal(diskName, EDIT, ARCHIVO, data->cont, data->path, "", -1, -1);
        free(name);
        free(cont);
    }
}

void catF(Cat *data) {
    for (int i = 0; i < 10; ++i) {
        if (stricmp((const char *) data->path[i], "") == 0)break;
        if (strlen((const char *) data->path[i]) == 0)break;
        // printf("%s\n",(char *) data->path[i]);

        int noinode = getFileFolder(diskName, (char *) data->path[i]);
        inode in;
        getInode(&in, diskName, noinode);
        bool tiene = 0;
        if (in.i_uid == idUser) {
            if (tienePermiso('r', 'u', in.i_perm))
                tiene = 1;
        } else if (idUser == 1) {
            tiene = 1;
        } else if (in.i_gid == idGrupo) {
            if (tienePermiso('r', 'g', in.i_perm))
                tiene = 1;
        } else {
            if (tienePermiso('r', 'o', in.i_perm))
                tiene = 1;
        }


        if (!tiene) {
            alert(ERR, "El usuario actual no tiene parmisos para leer este archivo");
        } else {
            char *destino = newString(280320);
            char *name = newString(12);

            int folder = getFolder(diskName, (char *) data->path[i], FALSE);
            getFileFolderName(name, (char *) data->path[i]);
            getFileContent(diskName, folder, name, destino);
            printf("# %s\n", destino);
        }
    }
}

void renameF(Ren *data) {

    if (data->valid == TRUE) {
        char *path = "";
        char *name = newString(12);
        int folder = 0;

        path = data->path;
        name = newString(12);


        folder = getFolder(diskName, path, FALSE);
        getFileFolderName(name, path);
        renameFileFolder(diskName, folder, name, data->name);
        addJournal(diskName, RENAME, -1, data->name, data->path, "", -1, -1);
        free(name);
    }
}

void cpFileFolder(Cp *data) {
    if (data->valid == TRUE) {
        copyFileFolder(diskName, (char *) data->path1, (char *) data->path2);
        addJournal(diskName, COPY, -1, "", (char *) data->path1, (char *) data->path2, -1, -1);
    }
}

void mvFileFolder(Mv *data) {
    if (data->valid == TRUE) {
        copyFileFolder(diskName, (char *) data->path1, (char *) data->path2);
        addJournal(diskName, COPY, -1, "", (char *) data->path1, (char *) data->path2, -1, -1);
        char *name = newString(12);
        int folder = getFolder(diskName, (char *) data->path1, FALSE);
        getFileFolderName(name, (char *) data->path1);
        removeFile(diskName, folder, name);
        free(name);
        addJournal(diskName, DELETE, 0, "", (char *) data->path1, "", 0, 0);
    }
}

void makeFolder(mkDir *data) {
    if (data->valid == TRUE) {
        char *path;
        char *name;
        int folder = 0;

        path = data->path;
        name = newString(12);


        folder = getFolder(diskName, path, data->padre);
        getFileFolderName(name, path);
        writeFolder(diskName, folder, name);
        //addJournal(diskName, CREATE, CARPETA, "", data->path, "", idUser, 755);
        addJournal(diskName, CREATE, CARPETA, "", data->path, "", idUser, 664);

        free(name);
    }
}

int copyFileFolder(char *id, char *pathF, char *pathD) {
    char *pathFuente = newString(PATH_LENGHT);
    char *pathDestino = newString(PATH_LENGHT);
    sprintf(pathFuente, "%s%c", pathF, '/');
    sprintf(pathDestino, "%s%c", pathD, '/');
    //printf("%s\n", pathFuente);
    //printf("%s\n", pathDestino);
    int inodeFuente = getFileFolder(id, pathFuente);
    int inodeDestino = getFileFolder(id, pathDestino);

    if (inodeFuente > -1) {
        if (inodeDestino > -1) {
            //printf("fuente: %d\n", inodeFuente);
            inode Fuente;
            getInode(&Fuente, id, inodeFuente);
            if (Fuente.i_type == '1') {
                char *destino = newString(280320);
                char *name = newString(12);

                int folder = getFolder(id, pathF, FALSE);
                getFileFolderName(name, pathF);
                getFileContent(id, folder, name, destino);
                //printf("contenido : \n%s\n", destino);



                inode Destino;
                getInode(&Destino, id, inodeDestino);
                if (Destino.i_type == '0') {
                    writeFile(id, inodeDestino, name, destino);
                } else {
                    printf("%s\n", "El directorio destino debe ser de tipo carpeta.");
                }
                free(name);
                free(destino);
            } else {
                folder(id, inodeFuente, pathFuente, pathDestino);
            }
        } else {
            printf("%s\n", "No se puede encontrar el directorio Destino de Archios");
        }
    } else {
        printf("%s\n", "No se puede encontrar el directorio Fuente de Archivos");
    }


    free(pathFuente);
    free(pathDestino);
}


void folder(char *id, int index, char *currentFolderF, char *currentFolderD) {
    inode In;
    getInode(&In, id, index);


    for (int i = 0; i <= 14; i++) {
        if (i <= 11) {
            // printf("añsfasksaljfñalskdjfaslkjdfñalksdjfñaklsjdflkasjdf\n");
            if (In.i_block[i] != -1) {
                bool first = FALSE;
                if (i == 0) {
                    first = TRUE;
                }
                //printf("añslkdjfañslkdjfasñldk\n");
                block(id, In.i_block[i], In.i_type, first, currentFolderF, currentFolderD);
            }
        }
    }
}

void block(char *id, int index, char type, bool first, char *currentFolderF, char *currentFolderD) {
    if (type == '0') {
        dBlock dB;
        getFolderBlock(&dB, id, index);

        int _i = 0;
        if (first) {
            _i = 2;
        }


        for (int i = _i; i <= 3; i++) {
            if (dB.b_content[i].b_inodo != -1) {
                char *tmp = newString(12);
                sprintf(tmp, "%.12s", dB.b_content[i].b_name);
                //printf("tmp: %s\n", tmp);


                char *currentFolderD2 = newString(500);
                char *currentFolderF2 = newString(500);
                sprintf(currentFolderD2, "%s%s", currentFolderD, tmp);
                sprintf(currentFolderF2, "%s%s", currentFolderF, tmp);
                inode aux;
                getInode(&aux, id, dB.b_content[i].b_inodo);
                if (aux.i_type == '1') {
                    /*printf("%s\n", "es archivito: ");
          printf("%s\n", currentFolderD2);
          printf("%s\n\n", currentFolderF2);*/

                    char *destino = newString(280320);
                    char *name = newString(12);

                    int f = getFolder(id, currentFolderF2, TRUE);
                    getFileFolderName(name, currentFolderF2);
                    getFileContent(id, f, name, destino);
                    //printf("contenido : \n%s\n", destino);



                    name = newString(12);

                    f = getFolder(id, currentFolderD2, TRUE);
                    getFileFolderName(name, currentFolderD2);
                    writeFile(id, f, name, destino);


                    free(destino);
                    free(name);

                } else {
                    /*printf("%s", "es folder: ");
          printf("%s\n", currentFolderD2);
          printf("%s\n\n", currentFolderF2);*/


                    char *name = newString(12);

                    int f = getFolder(id, currentFolderD2, TRUE);
                    getFileFolderName(name, currentFolderD2);
                    writeFolder(id, f, name);
                    free(name);

                    sprintf(currentFolderD2, "%s/", currentFolderD2);
                    sprintf(currentFolderF2, "%s/", currentFolderF2);
                    folder(id, dB.b_content[i].b_inodo, currentFolderF2, currentFolderD2);
                    i = 4;
                }


                free(currentFolderD2);
                free(tmp);
            }
        }
    } else if (type == '1') {
        /*fBlock fB;
    getFileBlock(&fB, id, index);

    printf("%.64s\n", fB.b_content);*/
    } else if (type == '2') {

    }
}

int changePermisos(char *id, char *path, int perms, bool recursivo) {
    char *path2 = newString(PATH_LENGHT);
    sprintf(path2, "%s/", path);
    int noinode = getFileFolder(id, path2);
    inode in;
    getInode(&in, id, noinode);
    bool tiene = 0;
    if (in.i_uid == idUser) {
        if (tienePermiso('w', 'u', in.i_perm)) {
            tiene = 1;
        }
    } else if (idUser == 1) {
        tiene = 1;
    }


    if (!tiene) {
        alert(ERR, "El usuario actual no tiene parmisos para cambiar el acceso a este archivo");
        return 0;
    }

    in.i_perm = perms;

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int inodeTablePos = getInodeTablePos(sBpointer, noinode);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, inodeTablePos, SEEK_SET);
        fwrite(&in, sizeof(inode), 1, fp);
        fclose(fp);
    }

    if (!recursivo)return 1;
    if (in.i_type == '1')return 0;
    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo != -1) {
                    if (stricmp(dTMP.b_content[j].b_name, ".") == 0) continue;
                    if (stricmp(dTMP.b_content[j].b_name, "..") == 0) continue;
                    char *tmp = newString(PATH_LENGHT);
                    sprintf(tmp, "%s/%s", path, dTMP.b_content[j].b_name);
                    //printf("%s\n",tmp);
                    changePermisos(id, tmp, perms, recursivo);
                }
            }
        }
    }
    return 1;
}

int changePropietario(char *id, char *path, int usr, int grp, bool recursivo) {
    char *path2 = newString(PATH_LENGHT);
    sprintf(path2, "%s/", path);
    int noinode = getFileFolder(id, path2);
    inode in;
    getInode(&in, id, noinode);
    bool tiene = 0;
    if (in.i_uid == idUser) {
        if (tienePermiso('w', 'u', in.i_perm)) {
            tiene = 1;
        }
    } else if (idUser == 1) {
        tiene = 1;
    }


    if (!tiene) {
        alert(ERR, "El usuario actual no tiene parmisos para cambiar el propietario a este archivo");
        return 0;
    }

    in.i_uid = usr;
    in.i_gid = grp;

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int inodeTablePos = getInodeTablePos(sBpointer, noinode);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, inodeTablePos, SEEK_SET);
        fwrite(&in, sizeof(inode), 1, fp);
        fclose(fp);
    }

    if (!recursivo)return 1;
    if (in.i_type == '1')return 0;
    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo != -1) {
                    if (stricmp(dTMP.b_content[j].b_name, ".") == 0) continue;
                    if (stricmp(dTMP.b_content[j].b_name, "..") == 0) continue;
                    char *tmp = newString(PATH_LENGHT);
                    sprintf(tmp, "%s/%s", path, dTMP.b_content[j].b_name);
                    //printf("%s\n",tmp);
                    changePropietario(id, tmp, usr, grp, recursivo);
                }
            }
        }
    }
    return 1;
}

int getFileFolder(char *id, char *path) {
    int index = 0;
    int limit = getLastSlash(path);
    bool encontrada = FALSE;
    inode in;
    int z = 0;
    if (limit != 0) {
        int start = 0;
        char tmp[12];
        while (start < limit) {
            for (int i = 0; i < 12; i++) {
                tmp[i] = '\0';
            }
            int index = 0;
            while (path[start] != '/') {
                tmp[index] = path[start];
                index++;
                start++;
            }
            if (start != 0) {
                //printf("%s\n", tmp);
                getInode(&in, id, z);

                for (int i = 0; i <= 11; i++) {
                    int indexTMP = in.i_block[i];
                    //printf("i[%d]: %d\n",i, indexTMP);
                    if (indexTMP != -1) {
                        dBlock dTMP;
                        getFolderBlock(&dTMP, id, indexTMP);
                        for (int j = 0; j <= 3; j++) {
                            if (dTMP.b_content[j].b_inodo != -1) {
                                if (strcmp(tmp, dTMP.b_content[j].b_name) == 0) {
                                    inode t;
                                    getInode(&t, id, dTMP.b_content[j].b_inodo);
                                    z = dTMP.b_content[j].b_inodo;
                                    j = 4;
                                    i = 12;
                                    encontrada = TRUE;
                                }
                            }
                        }
                    }
                }
                if (!encontrada) {
                    printf("No se pudo encontrar La carpeta o archivo especificado: %s\n", tmp);
                    z = -1;
                }
            }
            start++;

        }
    }
    return z;
}


int getFolder(char *id, char *path, bool father) {
    int index = 0;
    int limit = getLastSlash(path);
    bool encontrada = FALSE;
    inode in;
    int z = 0;
    if (limit != 0) {
        int start = 0;
        char tmp[12];
        while (start < limit) {
            for (int i = 0; i < 12; i++) {
                tmp[i] = '\0';
            }
            int index = 0;
            while (path[start] != '/') {
                tmp[index] = path[start];
                index++;
                start++;
            }
            encontrada = FALSE;
            if (start != 0) {
                //printf("%s\n", tmp);
                getInode(&in, id, z);

                for (int i = 0; i <= 11; i++) {
                    int indexTMP = in.i_block[i];
                    //printf("i[%d]: %d\n",i, indexTMP);
                    if (indexTMP != -1) {
                        dBlock dTMP;
                        getFolderBlock(&dTMP, id, indexTMP);
                        for (int j = 0; j <= 3; j++) {
                            if (dTMP.b_content[j].b_inodo != -1) {
                                //printf("%s\n", dTMP.b_content[j].b_name);
                                if (strcmp(tmp, dTMP.b_content[j].b_name) == 0) {
                                    inode t;
                                    getInode(&t, id, dTMP.b_content[j].b_inodo);
                                    if (t.i_type == '0') {
                                        //printf("%s\n", "-----------------------------");
                                        //printf("%s\n",dTMP.b_content[j].b_name);
                                        //printf("%d\n",dTMP.b_content[j].b_inodo);
                                        //printf("%s\n", "-----------------------------");
                                        z = dTMP.b_content[j].b_inodo;
                                        if (start == limit) {
                                            j = 4;
                                            i = 12;
                                        }
                                        encontrada = TRUE;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!encontrada) {
                    if (father) {
                        printf("No se pudo encontrar La carpeta especificada, se procedera a crear la carpeta: %s\n",
                               tmp);
                        z = writeFolder(id, z, tmp);
                        encontrada = FALSE;
                    } else {
                        printf("No se pudo encontrar La carpeta especificada: %s\n", tmp);
                        z = -1;
                    }
                } else {
                    // printf("aver\n");
                }
            }
            start++;

        }
    }
    return z;
}

void getFileFolderName(char *destiny, char *path) {
    int index = 0;
    int limit = getLastSlash(path);
    int start = limit + 1;
    char file_folder_name[12];
    for (int i = 0; i < 12; i++) {
        file_folder_name[i] = '\0';
    }
    while (start < strlen(path)) {
        file_folder_name[index] = path[start];
        index++;
        start++;
    }
    sprintf(destiny, "%.12s", file_folder_name);
}

int getLastSlash(char *path) {
    int start = 0;
    int ret = 0;
    while (start < strlen(path)) {
        if (path[start] == '/') {
            ret = start;
        }
        start++;
    }
    return ret;
}


int renameFileFolder(char *id, int inodeFolder, char *prevName, char *newName) {
    inode in;
    getInode(&in, id, inodeFolder);

    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo != -1) {
                    if (strcmp(prevName, dTMP.b_content[j].b_name) == 0) {
                        sprintf(dTMP.b_content[j].b_name, "%.12s", newName);
                        updateFolderBlock(&dTMP, id, indexTMP);
                        //j=4;
                        //i=12;
                        return 1;
                    }
                }
            }
        }
    }
    printf("No se pudo encontrar el directorio especificado");
    return 0;
}

int editFile(char *id, int inodeFolder, char *name, char *content) {
    inode in;
    getInode(&in, id, inodeFolder);

    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo != -1) {
                    if (strcmp(name, dTMP.b_content[j].b_name) == 0) {

                        // DELETE PREV BLOCKS

                        superBlock sB;
                        getSuperBlock(&sB, id);

                        inode _in;
                        getInode(&_in, id, dTMP.b_content[j].b_inodo);
                        for (int w = 0; w <= 11; w++) {
                            if (_in.i_block[w] != -1) {
                                writeBlockBit(id, _in.i_block[w], FALSE);
                                sB.s_first_blo = firstFreeBlock(id);
                                sB.s_free_blocks_count++;
                            }
                        }

                        disk *tmp = search(mounts->head, mounts->head, id);
                        if (tmp != NULL) {
                            int part_start = getPartStart(id);
                            FILE *fp = fopen(tmp->path, "r+b");
                            fseek(fp, part_start, SEEK_SET);
                            fwrite(&sB, sizeof(superBlock), 1, fp);
                            fclose(fp);
                        }

                        //writeInode(id, dTMP.b_content[j].b_inodo, idUser, idGrupo, 0, '1', 644);
                        writeInode(id, dTMP.b_content[j].b_inodo, idUser, idGrupo, 0, '1', 664);
                        int freeBlock = firstFreeBlock(id);
                        for (int i = 0; i < ceilgg(strlen(content) / (double) 64); i++) {
                            char *tmp = newString(64);
                            sprintf(tmp, "%.64s", &content[i * 64]);
                            updateInodeAD(id, dTMP.b_content[j].b_inodo, i, freeBlock);
                            writeFileBlock(id, freeBlock, tmp);
                            freeBlock = firstFreeBlock(id);
                            free(tmp);
                        }
                        //j=4;
                        //i=12;
                        return 1;
                    }
                }
            }
        }
    }
    printf("No se pudo encontrar el directorio especificado");
    return 0;
}

int removeFolder(char *id, int inodeFolder) {
    inode in;
    getInode(&in, id, inodeFolder);

    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            int zz = 0;
            if (i == 0)
                zz = 2;
            for (int j = zz; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo != -1) {
                    // DELETE PREV BLOCKS

                    superBlock sB;
                    getSuperBlock(&sB, id);

                    inode _in;
                    getInode(&_in, id, dTMP.b_content[j].b_inodo);
                    for (int w = 0; w <= 11; w++) {
                        if (_in.i_block[w] != -1) {
                            writeBlockBit(id, _in.i_block[w], FALSE);
                            sB.s_first_blo = firstFreeBlock(id);
                            sB.s_free_blocks_count++;
                            if (_in.i_type == '0') {
                                removeFolder(id, dTMP.b_content[j].b_inodo);
                            }
                        }
                    }

                    //verificar si esta vacio ya el bloque
                    writeInodeBit(id, dTMP.b_content[j].b_inodo, FALSE);
                    sB.s_first_ino = firstFreeBlock(id);
                    sB.s_free_inodes_count++;

                    removeFileFolderName(id, indexTMP, j);


                    disk *tmp = search(mounts->head, mounts->head, id);
                    if (tmp != NULL) {
                        int part_start = getPartStart(id);
                        FILE *fp = fopen(tmp->path, "r+b");
                        fseek(fp, part_start, SEEK_SET);
                        fwrite(&sB, sizeof(superBlock), 1, fp);
                        fclose(fp);
                    }
                }
            }
        }
    }
    return 0;
}

int removeFile(char *id, int inodeFolder, char *name) {
    inode in;
    getInode(&in, id, inodeFolder);


    bool tiene = 0;
    if (in.i_uid == idUser) {
        if (tienePermiso('w', 'u', in.i_perm))
            tiene = 1;
    } else if (idUser == 1) {
        tiene = 1;
    } else if (in.i_gid == idGrupo) {
        if (tienePermiso('w', 'g', in.i_perm))
            tiene = 1;
    } else {
        if (tienePermiso('w', 'o', in.i_perm))
            tiene = 1;
    }


    if (!tiene) {
        alert(ERR, "El usuario actual no tiene parmisos de escritura en este archivo");
        return 0;
    } else {


        for (int i = 0; i <= 11; i++) {
            int indexTMP = in.i_block[i];
            //printf("i[%d]: %d\n",i, indexTMP);
            if (indexTMP != -1) {
                dBlock dTMP;
                getFolderBlock(&dTMP, id, indexTMP);
                for (int j = 0; j <= 3; j++) {
                    if (dTMP.b_content[j].b_inodo != -1) {
                        if (strcmp(name, dTMP.b_content[j].b_name) == 0) {

                            // DELETE PREV BLOCKS

                            superBlock sB;
                            getSuperBlock(&sB, id);

                            inode _in;
                            getInode(&_in, id, dTMP.b_content[j].b_inodo);
                            for (int w = 0; w <= 11; w++) {
                                if (_in.i_block[w] != -1) {
                                    writeBlockBit(id, _in.i_block[w], FALSE);
                                    sB.s_first_blo = firstFreeBlock(id);
                                    sB.s_free_blocks_count++;
                                    if (_in.i_type == '0') {
                                        removeFolder(id, dTMP.b_content[j].b_inodo);
                                    }
                                }
                            }

                            //verificar si esta vacio ya el bloque
                            writeInodeBit(id, dTMP.b_content[j].b_inodo, FALSE);
                            sB.s_first_ino = firstFreeBlock(id);
                            sB.s_free_inodes_count++;

                            removeFileFolderName(id, indexTMP, j);


                            disk *tmp = search(mounts->head, mounts->head, id);
                            if (tmp != NULL) {
                                int part_start = getPartStart(id);
                                FILE *fp = fopen(tmp->path, "r+b");
                                fseek(fp, part_start, SEEK_SET);
                                fwrite(&sB, sizeof(superBlock), 1, fp);
                                fclose(fp);
                            }
                            return 1;
                        }
                    }
                }
            }
        }
    }
    printf("No se pudo encontrar el directorio especificado");
    return 0;
}

int removeFileFolderName(char *id, int blockIndex, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockTablePos = getBlockTablePos(sBpointer, blockIndex);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);

        dBlock block;
        getFolderBlock(&block, id, blockIndex);
        block.b_content[index].b_inodo = -1;
        sprintf(block.b_content[index].b_name, "%.12s", "\0\0\0\0\0\0\0\0\0\0\0\0");

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, blockTablePos, SEEK_SET);
        fwrite(&block, sizeof(fBlock), 1, fp);
        fclose(fp);
    }
}

int getFileContent(char *id, int inodeFolder, char *name, char *destino) {
    inode in;
    getInode(&in, id, inodeFolder);

    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo != -1) {
                    if (strcmp(name, dTMP.b_content[j].b_name) == 0) {

                        // DELETE PREV BLOCKS
                        inode _in;
                        getInode(&_in, id, dTMP.b_content[j].b_inodo);

                        for (int k = 0; k <= 11; k++) {
                            int _indexTMP = _in.i_block[k];
                            if (_indexTMP != -1) {
                                fBlock fB;
                                getFileBlock(&fB, id, _indexTMP);
                                sprintf(destino, "%s%.64s", destino, fB.b_content);
                            } else {
                                k = 12;
                            }
                        }

                        return 1;
                    }
                }
            }
        }
    }
    printf("No se pudo encontrar el directorio especificado");
    return 0;
}

int writeFolder(char *id, int inodeFolder, char *name) {
    inode in;
    getInode(&in, id, inodeFolder);

    int freeInode = firstFreeInode(id);
    int freeBlock = firstFreeBlock(id);

    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo == -1) {
                    sprintf(dTMP.b_content[j].b_name, "%.12s", name);
                    dTMP.b_content[j].b_inodo = freeInode;
                    updateFolderBlock(&dTMP, id, indexTMP);
                    //writeInode(id, freeInode, idUser, idGrupo, 0, '0', 755);
                    writeInode(id, freeInode, idUser, idGrupo, 0, '0', 664);
                    updateInodeAD(id, freeInode, 0, freeBlock);
                    writeFirstFolderBlock(id, freeBlock, inodeFolder, freeInode);
                    //j=4;
                    //i=12;
                    return freeInode;
                }
            }
        } else {
            updateInodeAD(id, inodeFolder, i, freeBlock);
            writeFolderBlock(id, freeBlock);
            writeFolder(id, inodeFolder, name);
            //i = 12;
            return freeInode;
        }
    }
    return freeInode;
}

int writeFile(char *id, int inodeFolder, char *name, char *content) {
    inode in;
    getInode(&in, id, inodeFolder);


    int freeInode = firstFreeInode(id);
    int freeBlock = firstFreeBlock(id);

    for (int i = 0; i <= 11; i++) {
        int indexTMP = in.i_block[i];
        //printf("i[%d]: %d\n",i, indexTMP);
        if (indexTMP != -1) {
            dBlock dTMP;
            getFolderBlock(&dTMP, id, indexTMP);
            for (int j = 0; j <= 3; j++) {
                if (dTMP.b_content[j].b_inodo == -1) {
                    sprintf(dTMP.b_content[j].b_name, "%.12s", name);
                    dTMP.b_content[j].b_inodo = freeInode;
                    updateFolderBlock(&dTMP, id, indexTMP);
                    //writeInode(id, freeInode, idUser, idGrupo, 0, '1', 644);
                    writeInode(id, freeInode, idUser, idGrupo, 0, '1', 664);

                    for (int i = 0; i < ceilgg(strlen(content) / (double) 64); i++) {
                        char *tmp = newString(64);
                        sprintf(tmp, "%.64s", &content[i * 64]);
                        updateInodeAD(id, freeInode, i, freeBlock);
                        writeFileBlock(id, freeBlock, tmp);
                        freeBlock = firstFreeBlock(id);
                        free(tmp);
                    }
                    //j=4;
                    //i=12;
                    return freeInode;
                }
            }
        } else {
            updateInodeAD(id, inodeFolder, i, freeBlock);
            writeFolderBlock(id, freeBlock);
            writeFile(id, inodeFolder, name, content);
            //i = 12;
            return freeInode;
        }
    }
    return freeInode;
}


void writeInode(char *id, int index, int i_uid, int i_gid, int i_size, char i_type, int i_perm) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int inodeBitMapPos = getInodeBitMapPos(sBpointer, index);
    int inodeTablePos = getInodeTablePos(sBpointer, index);
    /*printf("%d\n", inodeTablePos);
   printf("%d\n", inodeBitMapPos);*/

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);
        inode in;
        configInode(&in, i_uid, i_gid, i_size, i_type, i_perm);
        //printf("%d\n", in.i_size);

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, inodeTablePos, SEEK_SET);
        fwrite(&in, sizeof(inode), 1, fp);
        fseek(fp, inodeBitMapPos, SEEK_SET);
        char w = '1';
        fwrite(&w, sizeof(char), 1, fp);
        fclose(fp);

        fp = fopen(tmp->path, "r+b");
        sB.s_first_ino = firstFreeInode(id);
        sB.s_free_inodes_count--;
        fseek(fp, part_start, SEEK_SET);
        fwrite(&sB, sizeof(superBlock), 1, fp);
        fclose(fp);
    }
}

void writeFileBlock(char *id, int index, char *content) {

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockBitMapPos = getBlockBitMapPos(sBpointer, index);
    int blockTablePos = getBlockTablePos(sBpointer, index);
    /*printf("%d\n", blockTablePos);
  printf("%d\n", blockBitMapPos);*/


    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);

        fBlock block;
        configFileBlock(&block, content);
        //printf("%s\n", block.b_content);

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, blockTablePos, SEEK_SET);
        fwrite(&block, sizeof(fBlock), 1, fp);
        fseek(fp, blockBitMapPos, SEEK_SET);
        char w = '1';
        fwrite(&w, sizeof(char), 1, fp);
        fclose(fp);

        fp = fopen(tmp->path, "r+b");
        sB.s_first_blo = firstFreeBlock(id);
        sB.s_free_blocks_count--;
        fseek(fp, part_start, SEEK_SET);
        fwrite(&sB, sizeof(superBlock), 1, fp);
        fclose(fp);
    }
}

void writeFirstFolderBlock(char *id, int index, int padre, int me) {

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockBitMapPos = getBlockBitMapPos(sBpointer, index);
    int blockTablePos = getBlockTablePos(sBpointer, index);
    /*printf("%d\n", blockTablePos);
  printf("%d\n", blockBitMapPos);*/

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);

        dBlock block;
        configFirstFolderBlock(&block, padre, me);

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, blockTablePos, SEEK_SET);
        fwrite(&block, sizeof(fBlock), 1, fp);
        fseek(fp, blockBitMapPos, SEEK_SET);
        char w = '1';
        fwrite(&w, sizeof(char), 1, fp);
        fclose(fp);

        fp = fopen(tmp->path, "r+b");
        sB.s_first_blo = firstFreeBlock(id);
        sB.s_free_blocks_count--;
        fseek(fp, part_start, SEEK_SET);
        fwrite(&sB, sizeof(superBlock), 1, fp);
        fclose(fp);
    }
}

void writeFolderBlock(char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockBitMapPos = getBlockBitMapPos(sBpointer, index);
    int blockTablePos = getBlockTablePos(sBpointer, index);
    /*printf("%d\n", blockTablePos);
  printf("%d\n", blockBitMapPos);*/

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);

        dBlock block;
        configFolderBlock(&block);

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, blockTablePos, SEEK_SET);
        fwrite(&block, sizeof(fBlock), 1, fp);
        fseek(fp, blockBitMapPos, SEEK_SET);
        char w = '1';
        fwrite(&w, sizeof(char), 1, fp);
        fclose(fp);

        fp = fopen(tmp->path, "r+b");
        sB.s_first_blo = firstFreeBlock(id);
        sB.s_free_blocks_count--;
        fseek(fp, part_start, SEEK_SET);
        fwrite(&sB, sizeof(superBlock), 1, fp);
        fclose(fp);
    }
}

void writePointerBlock(char *id, int index) {

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockBitMapPos = getBlockBitMapPos(sBpointer, index);
    int blockTablePos = getBlockTablePos(sBpointer, index);
    /*printf("%d\n", blockTablePos);
  printf("%d\n", blockBitMapPos);*/

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        int part_size = getPartSize(id);
        int part_start = getPartStart(id);
        char *part_name = getPartName(id);

        pBlock block;
        configPointerBlock(&block);

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, blockTablePos, SEEK_SET);
        fwrite(&block, sizeof(pBlock), 1, fp);
        fseek(fp, blockBitMapPos, SEEK_SET);
        char w = '1';
        fwrite(&w, sizeof(char), 1, fp);
        fclose(fp);

        fp = fopen(tmp->path, "r+b");
        sB.s_first_blo = firstFreeBlock(id);
        sB.s_free_blocks_count--;
        fseek(fp, part_start, SEEK_SET);
        fwrite(&sB, sizeof(superBlock), 1, fp);
        fclose(fp);
    }
}

void writeJournal(char *id) {
    int min = getPartStart(id) + sizeof(superBlock);
    long max = 400 * sizeof(Jjjj);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        Jjjj tmp_;
        tmp_.typo_OP = -1;
        while (min < max) {
            fseek(fp, min, SEEK_SET);
            fwrite(&tmp_, sizeof(Jjjj), 1, fp);
            min += sizeof(Jjjj);
        }
        fclose(fp);
    }
}

void printJournal(char *id) {
    int min = getPartStart(id) + sizeof(superBlock);
    long max = min + 400 * sizeof(Jjjj);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        Jjjj tmp_;
        //printf("%d, %s",min,"\n");

        while (min < max) {
            fseek(fp, min, SEEK_SET);
            fread(&tmp_, sizeof(Jjjj), 1, fp);
            min += sizeof(Jjjj);
            switch (tmp_.typo_OP) {
                case CREATE:{
                    printf("crear");
                    if (tmp_.typo == ARCHIVO) {
                        char *path = "";
                        char *name = newString(12);
                        int folder = 0;
                        char *cont = newString(280320);

                        path = tmp_.path1;
                        name = newString(12);


                        folder = getFolder(id, path, TRUE);
                        getFileFolderName(name, path);
                        writeFile(id, folder, name, tmp_.cont);
                    } else {
                        char *path;
                        char *name;
                        int folder = 0;

                        path = tmp_.path1;
                        name = newString(12);


                        folder = getFolder(id, path, TRUE);
                        getFileFolderName(name, path);
                        writeFolder(id, folder, name);
                    }
                }
                    break;

                case DELETE: {
                    printf("borrar");
                    char *name = newString(12);
                    int folder = getFolder(id, tmp_.path1, FALSE);
                    getFileFolderName(name, tmp_.path1);
                    removeFile(id, folder, name);
                    free(name);
                }
                    break;

                case COPY: {
                    printf("copiar");
                    copyFileFolder(id, (char *) tmp_.path1, (char *) tmp_.path2);
                }
                    break;

                case EDIT: {
                    printf("editar");
                    char *path = "";
                    char *name = newString(12);
                    int folder = 0;
                    char *cont = newString(280320);

                    path = tmp_.path1;
                    name = newString(12);

                    folder = getFolder(id, path, FALSE);
                    getFileFolderName(name, path);
                    editFile(id, folder, name, cont);
                }
                    break;

                case RENAME: {
                    printf("rename");
                    char *path = "";
                    char *name = newString(12);
                    int folder = 0;
                    path = tmp_.path1;
                    name = newString(12);

                    folder = getFolder(id, path, FALSE);
                    getFileFolderName(name, path);
                    renameFileFolder(id, folder, name, tmp_.cont);
                }
                    break;

                default:
                    break;
            }
             /*printf("----------------------------------\n");
      printf("%d", tmp_.typo_OP);
      printf("%d", tmp_.typo);
      printf("----------------------------------\n");*/
        }
        fclose(fp);
    }else {
        alert(ERR, "La particion Solicitada NO se encuentra MONTADA en el sistema");
    }
}

void addJournal(char *id, int typo_OP,
                int typo,
                char *cont,
                char *path1,
                char *path2,
                int propietario,
                int permisos) {
    Jjjj tmp;
    tmp.typo_OP = typo_OP;
    tmp.typo = typo;
    strcpy(tmp.cont, cont);
    getDate(tmp.fecha);
    strcpy(tmp.path1, path1);
    strcpy(tmp.path2, path2);
    tmp.propietario = propietario;
    tmp.permisos = permisos;

    int min = getPartStart(id) + sizeof(superBlock);
    long max = 400 * sizeof(Jjjj);

    disk *tmp_ = search(mounts->head, mounts->head, id);
    if (tmp_ != NULL) {
        FILE *fp;
        fp = fopen(tmp_->path, "r+b");
        Jjjj tmp__;
        while (min < max) {
            fseek(fp, min, SEEK_SET);
            fread(&tmp__, sizeof(Jjjj), 1, fp);
            if (tmp__.typo_OP != -1)
                min += sizeof(Jjjj);
            if (tmp__.typo_OP == -1)
                break;
        }
        fseek(fp, min, SEEK_SET);
        fwrite(&tmp, sizeof(Jjjj), 1, fp);
        fclose(fp);
    }
}

void writeInodeBitMap(char *id) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int min = getInodeBitMapPos(sBpointer, 0);
    int max = getInodeCount(sBpointer);
    max += min;
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        long int pos;
        fp = fopen(tmp->path, "r+b");
        while (min < max) {
            char tmp = '0';
            fseek(fp, min, SEEK_SET);
            fwrite(&tmp, sizeof(char), 1, fp);
            min++;
        }
        fclose(fp);
    }
}

void writeBlockBitMap(char *id) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int min = getBlockBitMapPos(sBpointer, 0);
    int max = getBlockCount(sBpointer);
    max += min;
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        long int pos;
        fp = fopen(tmp->path, "r+b");
        while (min < max) {
            char tmp = '0';
            fseek(fp, min, SEEK_SET);
            fwrite(&tmp, sizeof(char), 1, fp);
            min++;
        }
        fclose(fp);
    }
}

void updateInodeAD(char *id, int index, int AD, int value) {
    inode in;
    getInode(&in, id, index);
    in.i_block[AD] = value;

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int inodeTablePos = getInodeTablePos(sBpointer, index);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, inodeTablePos, SEEK_SET);
        fwrite(&in, sizeof(inode), 1, fp);
        fclose(fp);
    }
}

void updateFolderBlock(dBlock *dB, char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockTablePos = getBlockTablePos(sBpointer, index);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, blockTablePos, SEEK_SET);
        fwrite(dB, sizeof(fBlock), 1, fp);
        fclose(fp);
    }
}

int firstFreeInode(char *id) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int min = getInodeBitMapPos(sBpointer, 0);
    int start = min;
    int max = getInodeCount(sBpointer);
    max += min;
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        long int pos;
        fp = fopen(tmp->path, "r+b");
        while (min < max) {
            char tmp;
            fseek(fp, min, SEEK_SET);
            fread(&tmp, sizeof(char), 1, fp);
            if (tmp == '0')
                return min - start;
            min++;
        }
        fclose(fp);
    }
}

int firstFreeBlock(char *id) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int min = getBlockBitMapPos(sBpointer, 0);
    int start = min;
    int max = getBlockCount(sBpointer);
    max += min;
    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        long int pos;
        fp = fopen(tmp->path, "r+b");
        while (min < max) {
            char tmp;
            fseek(fp, min, SEEK_SET);
            fread(&tmp, sizeof(char), 1, fp);
            if (tmp == '0')
                return min - start;
            min++;
        }
        fclose(fp);
    }
}


void writeInodeBit(char *id, int index, bool status) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int pos = getInodeBitMapPos(sBpointer, index);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        char z = ' ';
        if (status == TRUE) {
            z = '1';
        } else {
            z = '0';
        }
        fseek(fp, pos, SEEK_SET);
        fwrite(&z, sizeof(char), 1, fp);
        fclose(fp);
    }
}

void writeBlockBit(char *id, int index, bool status) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int pos = getBlockBitMapPos(sBpointer, index);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        char z = ' ';
        if (status == TRUE) {
            z = '1';
        } else {
            z = '0';
        }
        fseek(fp, pos, SEEK_SET);
        fwrite(&z, sizeof(char), 1, fp);
        fclose(fp);
    }
}


char getInodeBit(char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int pos = getInodeBitMapPos(sBpointer, index);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        char z = ' ';
        fseek(fp, pos, SEEK_SET);
        fread(&z, sizeof(char), 1, fp);
        fclose(fp);
        return z;
    }
}

char getBlockBit(char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int pos = getBlockBitMapPos(sBpointer, index);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        char z = ' ';
        fseek(fp, pos, SEEK_SET);
        fread(&z, sizeof(char), 1, fp);
        fclose(fp);
        return z;
    }
}


void printInodeBitMap(char *id, int perLine, char deliMeter, char *path) {
    printf(CYN "########## P R I N T I N G     I N O D E    B I T    M A P ##########\n" RESET);
    printf("%s\n", "");
    FILE *rep;
    rep = fopen(path, "wb");
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int min = getInodeBitMapPos(sBpointer, 0);
    int max = getInodeCount(sBpointer);
    //printf("count: %d\n", max);

    max += min;
    //printf("min: %d\n", min);
    //printf("max: %d\n", max);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        long int pos;
        fp = fopen(tmp->path, "r+b");
        int a = 0;
        while (min < max) {
            char tmp;
            fseek(fp, min, SEEK_SET);
            fread(&tmp, sizeof(char), 1, fp);
            if (a < perLine) {
                if ((pos = ftell(fp)) != EOF)
                    fprintf(rep, "%c", tmp);
                a++;
            }
            if (a == perLine) {
                a = 0;
                fprintf(rep, "\n");
            } else {
                fprintf(rep, "%c", deliMeter);;
            }
            min++;
        }
        fclose(fp);
    }
    fclose(rep);
}

void printBlockBitMap(char *id, int perLine, char deliMeter, char *path) {
    printf(CYN "########## P R I N T I N G     B L O C K    B I T    M A P ##########\n" RESET);
    printf("%s\n", "");
    FILE *rep;
    rep = fopen(path, "wb");
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;


    int min = getBlockBitMapPos(sBpointer, 0);
    int max = getBlockCount(sBpointer);

    //printf("count: %d\n", max);

    max += min;
    //printf("min: %d\n", min);
    //printf("max: %d\n", max);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        FILE *fp;
        long int pos;
        fp = fopen(tmp->path, "r+b");
        int a = 0;
        while (min < max) {
            char tmp;
            fseek(fp, min, SEEK_SET);
            fread(&tmp, sizeof(char), 1, fp);
            if (a < perLine) {
                if ((pos = ftell(fp)) != EOF)
                    fprintf(rep, "%c", tmp);
                a++;
            }
            if (a == perLine) {
                a = 0;
                fprintf(rep, "\n");
            } else {
                fprintf(rep, "%c", deliMeter);;
            }
            min++;
        }
        fclose(fp);
    }
    fclose(rep);
}

void printSuperBlock(char *id) {
    printf("\n\n%s\n", "P R I N T I N G     S U P E R B L O C K");
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    printf("file system type: %d\n", sB.s_filesystem_type);
    printf("inodes count: %d\n", sB.s_inodes_count);
    printf("blocks count: %d\n", sB.s_blocks_count);
    printf("free inodes count: %d\n", sB.s_free_inodes_count);
    printf("free blocks count: %d\n", sB.s_free_blocks_count);
    printf("ultima fecha montado: %s\n", sB.s_mtime);
    printf("ultima fecha desmontado: %s\n", sB.s_umtime);
    printf("veces montado: %d\n", sB.s_mnt_count);
    printf("numero magico: %d\n", sB.s_magic);
    printf("tamaño inode: %d\n", sB.s_inode_size);
    printf("tamaño block: %d\n", sB.s_block_size);
    printf("primer inode libre: %d\n", sB.s_first_ino);
    printf("primer block libre: %d\n", sB.s_first_blo);
    printf("inicio bitmap inode: %d\n", sB.s_bm_inode_start);
    printf("inicio bitmap block: %d\n", sB.s_bm_block_start);
    printf("inicio table inode: %d\n", sB.s_inode_start);
    printf("inicio table block: %d\n", sB.s_block_start);
}

void printInode(char *id, int index) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int inodeTablePos = getInodeTablePos(sBpointer, index);
    //printf("%d\n", inodeTablePos);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        inode in;

        FILE *fp;
        fp = fopen(tmp->path, "r+b");
        fseek(fp, inodeTablePos, SEEK_SET);
        fread(&in, sizeof(inode), 1, fp);
        fclose(fp);

        printf("\n//////////////////// I N O D E [%d] ////////////////////\n", index);
        printf("i_uid: %d\n", in.i_uid);
        printf("i_gid: %d\n", in.i_gid);
        printf("i_size: %d\n", in.i_size);
        printf("i_type: %c\n", in.i_type);
        printf("i_perm: %d\n", in.i_perm);
        printf("i_atime: %s\n", in.i_atime);
        printf("i_ctime: %s\n", in.i_ctime);
        printf("i_mtime: %s\n", in.i_mtime);
        for (int i = 0; i < 15; i++) {
            printf("i_block[%d]: %d\n", i, in.i_block[i]);
        }
        printf("%s\n", "");
    }
}

void printBlock(char *id, int index, char type) {
    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    sBpointer = &sB;

    int blockTablePos = getBlockTablePos(sBpointer, index);
    //printf("%d\n", blockTablePos);

    disk *tmp = search(mounts->head, mounts->head, id);
    if (tmp != NULL) {
        if (type == 'd') {
            dBlock block;     //folder
            FILE *fp;
            fp = fopen(tmp->path, "r+b");
            fseek(fp, blockTablePos, SEEK_SET);
            fread(&block, sizeof(fBlock), 1, fp);
            fclose(fp);
            printf("\n////////////////// d - B L O C K [%d] //////////////////\n", index);
            for (int i = 0; i < 4; i++) {
                printf("b_content [%d].b_name: %s\n", i, block.b_content[i].b_name);
                printf("b_content [%d].b_inodo: %d\n", i, block.b_content[i].b_inodo);
                printf("%s\n", "");
            }
        } else if (type == 'f') {
            fBlock block;     //file
            FILE *fp;
            fp = fopen(tmp->path, "r+b");
            fseek(fp, blockTablePos, SEEK_SET);
            fread(&block, sizeof(fBlock), 1, fp);
            fclose(fp);
            printf("\n////////////////// f - B L O C K [%d] //////////////////\n", index);
            printf("b_content: %s\n", block.b_content);
        } else if (type == 'p') {
            pBlock block;     //pointer
            FILE *fp;
            fp = fopen(tmp->path, "r+b");
            fseek(fp, blockTablePos, SEEK_SET);
            fread(&block, sizeof(fBlock), 1, fp);
            fclose(fp);
            printf("\n////////////////// p - B L O C K [%d] //////////////////\n", index);
            for (int i = 0; i < 16; i++) {
                printf("b_pointer[%d]: %d\n", i, block.b_pointer[i]);
            }
        }
        printf("%s\n", "");
    }
}


int getGroups(char *file, char *destino) {
    int start = 0;
    int end = (int) strlen(file);
    while (start < end) {
        char *tmp = newString(15);
        sprintf(tmp, "%.15s", &file[start]);
        if (tmp[2] == 'G') {
            //printf("Se encontro Grupo:  %.15s", tmp);
            sprintf(destino, "%s%.15s", destino, tmp);
            start += 15;
        } else if (tmp[2] == 'U') {
            //printf("Se encontro Usuario:  %.37s", tmp);
            start += 37;
        }
        free(tmp);
    }
    return 1;
}

int getAllUsers(char *file, char *destino) {
    int start = 0;
    int end = (int) strlen(file);
    while (start < end) {
        char *tmp = newString(37);
        sprintf(tmp, "%.37s", &file[start]);
        if (tmp[2] == 'G') {
            //printf("Se encontro Grupo:  %.15s", tmp);
            //sprintf(destino, "%s%.15s", destino, tmp);
            start += 15;
        } else if (tmp[2] == 'U') {
            //printf("Se encontro Usuario:  %.37s", tmp);
            sprintf(destino, "%s%.37s", destino, tmp);
            start += 37;
        }
        free(tmp);
    }
    return 1;
}

int getUsers(char *file, char *g, char *destino) {
    int start = 0;
    int end = (int) strlen(file);
    char *group = newString(10);
    sprintf(group, "%.10s", g);
    for (int i = strlen(g); i < 10; i++) {
        group[i] = ' ';
    }
    while (start < end) {
        char *tmp = newString(37);
        sprintf(tmp, "%.37s", &file[start]);
        if (tmp[2] == 'G') {
            //printf("Se encontro Grupo:  %.15s", tmp);
            //sprintf(destino, "%s%.15s", destino, tmp);
            start += 15;
        } else if (tmp[2] == 'U') {
            //printf("Se encontro Usuario:  %.37s", tmp);
            char *groupTMP = newString(10);
            sprintf(groupTMP, "%.10s", &tmp[4]);
            if (stricmp(group, groupTMP) == 0) {
                sprintf(destino, "%s%.37s", destino, tmp);
            }
            start += 37;
            free(groupTMP);
        }
        free(tmp);
    }
    free(group);
    return 1;
}


int findGroupID(char *file, char *g) {
    char *group = newString(10);
    sprintf(group, "%.10s", g);
    for (int i = strlen(g); i < 10; i++) {
        group[i] = ' ';
    }
    char *users = newString(200000);
    getGroups(file, users);
    //printf(users);
    int start = 0;
    int end = strlen(users);
    while (start < end) {
        char *userTMP = newString(10);
        sprintf(userTMP, "%.10s", &users[start + 4]);
        int aux = 0;
        for (int i = 0; i < 10; i++) {
            if (group[i] != userTMP[i]) {
                aux++;
            }
        }
        //printf("aux: %d\n", aux);
        if (aux == 0) {
            int r = users[start] - '0';
            free(userTMP);
            free(users);
            free(group);
            return r;
        }
        //printf("%s\n", userTMP);
        start += 15;
        free(userTMP);
    }
    free(users);
    free(group);
    return -1;
}

int findUserID(char *users, char *usr) {
    char *user = newString(10);
    sprintf(user, "%.10s", usr);
    for (int i = strlen(usr); i < 10; i++) {
        user[i] = ' ';
    }
    //printf(users);
    int start = 0;
    int end = strlen(users);
    while (start < end) {
        char *userTMP = newString(10);
        sprintf(userTMP, "%.10s", &users[start + 15]);
        int aux = 0;
        for (int i = 0; i < 10; i++) {
            if (user[i] != userTMP[i]) {
                aux++;
            }
        }
        //printf("aux: %d\n", aux);
        if (aux == 0) {
            int r = users[start] - '0';
            free(userTMP);
            free(users);
            return r;
        }
        //printf("%s\n", userTMP);
        start += 37;
        free(userTMP);
    }
    free(users);
    return -1;
}

void findUserGroup(char *users, char *usr, char *destino) {
    char *user = newString(10);
    sprintf(user, "%.10s", usr);
    for (int i = strlen(usr); i < 10; i++) {
        user[i] = ' ';
    }
    //printf(users);
    int start = 0;
    int end = strlen(users);
    while (start < end) {
        char *userTMP = newString(10);
        sprintf(userTMP, "%.10s", &users[start + 15]);
        int aux = 0;
        for (int i = 0; i < 10; i++) {
            if (user[i] != userTMP[i]) {
                aux++;
            }
        }
        //printf("aux: %d\n", aux);
        if (aux == 0) {
            //ñaslkdjfañsldkjfañsldkfjñasldkjfñasdlkjfñasldkjfñalskdjf
            sprintf(destino, "%.10s", &users[start + 4]);
            start = end;
        }
        //printf("%s\n", userTMP);
        start += 37;
        free(userTMP);
    }
}

void findUserPsswd(char *users, char *usr, char *destino) {
    char *user = newString(10);
    sprintf(user, "%.10s", usr);
    for (int i = strlen(usr); i < 10; i++) {
        user[i] = ' ';
    }
    //printf(users);
    int start = 0;
    int end = strlen(users);
    while (start < end) {
        char *userTMP = newString(10);
        sprintf(userTMP, "%.10s", &users[start + 15]);
        int aux = 0;
        for (int i = 0; i < 10; i++) {
            if (user[i] != userTMP[i]) {
                aux++;
            }
        }
        //printf("aux: %d\n", aux);
        if (aux == 0) {
            //ñaslkdjfañsldkjfañsldkfjñasldkjfñasdlkjfñasldkjfñalskdjf
            sprintf(destino, "%.10s", &users[start + 26]);
            start = end;
        }
        //printf("%s\n", userTMP);
        start += 37;
        free(userTMP);
    }
}

int nextUserID(char *file) {
    char *users = newString(200000);
    getAllUsers(file, users);
    int start = 0;
    int end = strlen(users);
    char index = '\0';
    while (start < end) {
        char id = users[start];
        if (id > index) {
            index = id;
        }
        start += 37;
    }
    free(users);
    return index + 1;
}

int nextGroupID(char *file) {
    char *users = newString(200000);
    getGroups(file, users);
    int start = 0;
    int end = strlen(users);
    char index = '\0';
    while (start < end) {
        char id = users[start];
        if (id > index) {
            index = id;
        }
        start += 15;
    }
    free(users);
    return index + 1;
}


int addGroup(char *file, char *name, char *newFile) {
    char *gname = newString(10);
    sprintf(gname, "%.10s", name);
    for (int i = strlen(name); i < 10; i++) {
        gname[i] = ' ';
    }
    sprintf(newFile, "%s%c,G,%.10s\n", file, nextGroupID(file), gname);

    free(gname);
}


int removeGroup(char *file, char *n, char *newFile) {
    char copy[strlen(file)];
    strcpy(copy, file);
    int idG = findGroupID(file, n) + '0';
    for (int i = 0; i < strlen(file); i++) {
        if (file[i] == idG && file[i + 1] == ',' && file[i + 2] == 'G') {
            copy[i] = '0';
            strcpy(newFile, copy);
            return 1;
        }
    }
    return 0;
}

int groupIDindex(char *file, char *n, char *newFile) {
    char *name = newString(10);
    sprintf(name, "%.10s", n);
    for (int i = strlen(name); i < 10; i++) {
        name[i] = ' ';
    }
    int start = 0;
    int end = (int) strlen(file);
    while (start < end) {
        char *tmp = newString(15);
        sprintf(tmp, "%.15s", &file[start]);
        if (tmp[2] == 'G') {
            printf("Se encontro Grupo:  %.15s", tmp);
            printf("%.15s\n", tmp);
            char *tmpName = newString(10);
            sprintf(tmpName, "%.10s", &tmp[4]);
            if (strcmp(name, tmpName) == 0) {
                free(tmpName);
                free(tmp);
                free(name);
                return start;
            }
            free(tmpName);
            start += 15;
        } else if (tmp[2] == 'U') {
            //printf("Se encontro Usuario:  %.37s", tmp);
            start += 37;
        }
        free(tmp);
    }
    free(name);
    return -1;
}

int addUser(char *file, char *group, char *name, char *psswd, char *newFile) {
    char *ugroup = newString(10);
    sprintf(ugroup, "%.10s", group);
    for (int i = strlen(group); i < 10; i++) {
        ugroup[i] = ' ';
    }

    char *uname = newString(10);
    sprintf(uname, "%.10s", name);
    for (int i = strlen(name); i < 10; i++) {
        uname[i] = ' ';
    }

    char *upsswd = newString(10);
    sprintf(upsswd, "%.10s", psswd);
    for (int i = strlen(psswd); i < 10; i++) {
        upsswd[i] = ' ';
    }

    sprintf(newFile, "%s%c,U,%.10s,%.10s,%.10s\n", file, nextUserID(file), ugroup, uname, upsswd);
}

int removeUser(char *file, char *n, char *newFile) {
    char copy[strlen(file)];
    strcpy(copy, file);

    char *users = newString(1000);
    getAllUsers(file, users);
    int idG = findUserID(users, n) + '0';
    for (int i = 0; i < strlen(file); i++) {
        if (file[i] == idG && file[i + 1] == ',' && file[i + 2] == 'U') {
            copy[i] = '0';
            strcpy(newFile, copy);
            return 1;
        }
    }
    return 0;
}

void logout() {
    idUser = -1;
    idGrupo = -1;
}

void logIn(char *file, char *user, char *psswd) {
    int userID = 0;
    int groupID = 0;

    char *users = newString(300000);
    getAllUsers(file, users);
    userID = findUserID(users, user);
    if (userID > 0) {
        char *users = newString(300000);
        getAllUsers(file, users);
        char *grupo = newString(10);
        findUserGroup(users, user, grupo);
        groupID = findGroupID(file, grupo);
        free(grupo);

        char *psswdF = newString(10);
        char *psswdAux = newString(10);
        sprintf(psswdAux, "%s", psswd);
        for (int i = strlen(psswdAux); i < 10; i++) {
            psswdAux[i] = ' ';
        }
        findUserPsswd(users, user, psswdF);
        if (strcmp(psswdF, psswdAux) == 0) {
            idGrupo = groupID;
            idUser = userID;
        } else {
            printf("ERROR al AUTENTICAR, contraseña INCORRECTA\n");
            idGrupo = -1;
            idUser = -1;
        }
    } else {
        printf("ERROR al AUTENTICAR, usuario INCORRECTO\n");
        idGrupo = -1;
        idUser = -1;
    }
}
