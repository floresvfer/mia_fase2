#include "utils_gpvz.h"

//sudo apt install python-pydot python-pydot-ng graphviz

bool grapbSB(superBlock *sB, char *path) {
    printf(CYN "#############  C R E A T I N G    S U P E R   B L O C K     R E P  ############\n\n" RESET);
    FILE *fp;
    fp = fopen("/home/flores08/Desktop/reps/sb_rep.gv", "wb");

    fprintf(fp, "%s \n\n", "digraph example {");
    fprintf(fp, "%s\n", "graph [bgcolor=\"#000000\"];");
    //fprintf(fp, "%s \n", "node [shape=plaintext fontname="FreeMono"]");
    fprintf(fp, "%s \n", "node [shape=plaintext fontname=\"Ubuntu Condensed\"]");
    fprintf(fp, "%s \n", "rankdir=TB");

    fprintf(fp, "\t %s \n", "B [");
    fprintf(fp, "\t\t%s \n", "label=<");

    fprintf(fp, "\t\t\t%s \n", "<TABLE border=\"0\" cellspacing=\"1\" cellpadding=\"8\">");

    fprintf(fp, "\t\t\t\t%s \n", "<TR PORT=\"header\">");
    fprintf(fp,
            "\t\t\t\t\t<TD BGCOLOR=\"#FB0093\"  width=\"400\" COLSPAN=\"2\"><font color=\"#FFFFFF\">SUPER BLOCK</font></TD> \n");
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#1FD55F\"><font color=\"#2C1438\">Nombre</font></TD><TD align=\"left\"  BGCOLOR=\"#1FD55F\"><font color=\"#2C1438\">Valor</font></TD>\n");
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_inodes_count</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_inodes_count);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_blocks_count</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_blocks_count);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_free_inodes_count</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_free_inodes_count);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_free_blocks_count</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_free_blocks_count);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_mtime</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%s</font></TD>\n",
            sB->s_mtime);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_umtime</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%s</font></TD>\n",
            sB->s_umtime);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_mnt_count</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_mnt_count);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_magic</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_magic);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_inode_size</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_inode_size);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_block_size</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_block_size);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_first_ino</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_first_ino);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_first_blo</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_first_blo);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");


    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_bm_inode_start</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_bm_inode_start);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_bm_block_start</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_bm_block_start);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_inode_start</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_inode_start);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");


    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">s_block_start</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            sB->s_block_start);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");


    fprintf(fp, "\t\t\t%s \n", "</TABLE>");


    fprintf(fp, "\t\t%s \n", ">");
    fprintf(fp, "\t%s \n", "];");
    fprintf(fp, "%s \n", "}");


    fclose(fp);

    char *output = newString(300);
    char *local_file = path;

    char *ts1 = strdup(local_file);
    char *dir = dirname(ts1);

    char *mkDir = newString(100);
    sprintf(mkDir, "mkdir -p \"%s\"/", dir);
    system(mkDir);
    free(mkDir);

    sprintf(output, "dot -Tpng -o %s /home/flores08/Desktop/reps/sb_rep.gv", path);
    system(output);
    free(output);
}

bool graphMbr(Mbr *mbr, char *path) {
    Partition *array[4];
    for (int i = 0; i < 4; i++) {
        array[i] = &(mbr->mbr_partition[i]);
    }
    Partition *(*p)[] = &array;

    FILE *fp;
    fp = fopen("/home/flores08/Desktop/reps/mbr_rep.gv", "wb");

    fprintf(fp, "%s \n\n", "digraph example {");
    fprintf(fp, "%s\n", "graph [bgcolor=\"#000000\"];");
    //fprintf(fp, "%s \n", "node [shape=plaintext fontname="FreeMono"]");
    fprintf(fp, "%s \n", "node [shape=plaintext fontname=\"Ubuntu Condensed\"]");
    fprintf(fp, "%s \n", "rankdir=TB");

    fprintf(fp, "\t %s \n", "B [");
    fprintf(fp, "\t\t%s \n", "label=<");

    fprintf(fp, "\t\t\t%s \n", "<TABLE border=\"0\" cellspacing=\"1\" cellpadding=\"8\">");

    fprintf(fp, "\t\t\t\t%s \n", "<TR PORT=\"header\">");
    fprintf(fp,
            "\t\t\t\t\t<TD BGCOLOR=\"#FB0093\"  width=\"400\" COLSPAN=\"2\"><font color=\"#FFFFFF\">MBR</font></TD> \n");
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#1FD55F\"><font color=\"#2C1438\">Nombre</font></TD><TD align=\"left\"  BGCOLOR=\"#1FD55F\"><font color=\"#2C1438\">Valor</font></TD>\n");
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">mbr_tamanio</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            mbr->mbr_tamanio);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">mbr_fecha_creacion</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%s</font></TD>\n",
            mbr->mbr_fecha_creacion);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");
    fprintf(fp,
            "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">mbr_disk_signature</font></TD> <TD align=\"left\" BGCOLOR=\"#44CAA8\"><font color=\"#14091A\">%d</font></TD>\n",
            mbr->mbr_disk_signature);
    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    for (int i = 0; i < 4; i++) {
        Partition *partition = (*p)[i];

        if (partition->part_status == PARTITION_STATUS_ACTIVE) {
            fprintf(fp, "\t\t\t\t%s \n", "<TR>");
            fprintf(fp,
                    "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_status_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%c</font></TD>\n",
                    (i + 1), partition->part_status);
            fprintf(fp, "\t\t\t\t%s \n", "</TR>");

            fprintf(fp, "\t\t\t\t%s \n", "<TR>");
            fprintf(fp,
                    "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_type_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%c</font></TD>\n",
                    (i + 1), partition->part_type);
            fprintf(fp, "\t\t\t\t%s \n", "</TR>");

            fprintf(fp, "\t\t\t\t%s \n", "<TR>");
            fprintf(fp,
                    "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_fit_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%c%c</font></TD>\n",
                    (i + 1), partition->part_fit[0], partition->part_fit[1]);
            fprintf(fp, "\t\t\t\t%s \n", "</TR>");

            fprintf(fp, "\t\t\t\t%s \n", "<TR>");
            fprintf(fp,
                    "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_start_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%d</font></TD>\n",
                    (i + 1), partition->part_start);
            fprintf(fp, "\t\t\t\t%s \n", "</TR>");

            fprintf(fp, "\t\t\t\t%s \n", "<TR>");
            fprintf(fp,
                    "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_size_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%d</font></TD>\n",
                    (i + 1), partition->part_size);
            fprintf(fp, "\t\t\t\t%s \n", "</TR>");

            fprintf(fp, "\t\t\t\t%s \n", "<TR>");
            fprintf(fp,
                    "\t\t\t\t\t<TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">part_name_%d</font></TD> <TD align=\"left\" BGCOLOR=\"#14091A\"><font color=\"#44CAA8\">%s</font></TD>\n",
                    (i + 1), &(partition->part_name));
            fprintf(fp, "\t\t\t\t%s \n", "</TR>");

        }

    }

    fprintf(fp, "\t\t\t%s \n", "</TABLE>");


    fprintf(fp, "\t\t%s \n", ">");
    fprintf(fp, "\t%s \n", "];");
    fprintf(fp, "%s \n", "}");


    fclose(fp);

    char *output = newString(300);
    char *local_file = path;

    char *ts1 = strdup(local_file);
    char *dir = dirname(ts1);

    char *mkDir = newString(100);
    sprintf(mkDir, "mkdir -p \"%s\"/", dir);
    system(mkDir);
    free(mkDir);

    sprintf(output, "dot -Tpng -o %s /home/flores08/Desktop/reps/mbr_rep.gv", path);
    system(output);
    free(output);
}

bool graphDisk(Mbr *mbr, char *path) {
    Partition *array[4];
    for (int i = 0; i < 4; i++) {
        array[i] = &(mbr->mbr_partition[i]);
    }
    Partition *(*p)[] = &array;

    FILE *fp;
    fp = fopen("/home/flores08/Desktop/reps/disk_rep.gv", "wb");

    fprintf(fp, "%s \n\n", "digraph example {");
    fprintf(fp, "%s\n", "graph [bgcolor=\"#000000\"];");
    //fprintf(fp, "%s \n", "node [shape=plaintext fontname="FreeMono"]");
    fprintf(fp, "%s \n", "node [shape=plaintext fontname=\"Ubuntu Condensed\"]");
    fprintf(fp, "%s \n", "rankdir=TB");

    fprintf(fp, "\t %s \n", "B [");
    fprintf(fp, "\t\t%s \n", "label=<");

    fprintf(fp, "\t\t\t%s \n", "<TABLE border=\"1\" cellspacing=\"0\">");

    fprintf(fp, "\t\t\t\t%s \n", "<TR>");

    fprintf(fp, "\t\t\t\t\t%s \n",
            "<TD align=\"center\" width=\"100\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#FB0093\"><font color=\"#ffffff\">MBR</font></TD>");
    int lastIndex = 0;
    for (int i = 0; i < 4; i++) {
        Partition *partition = (*p)[i];
        Partition *partitionAux;

        if (partition->part_status == PARTITION_STATUS_ACTIVE) {
            if (i == 0 && partition->part_start != 136) {

                fprintf(fp,
                        "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#14091A\"><font color=\"#1FD55F\">LIBRE %.2f%c</font></TD>\n",
                        (int) getScale(mbr->mbr_tamanio, freeSpace(136, partition->part_start)),
                        ((getScale(mbr->mbr_tamanio, freeSpace(136, partition->part_start)) / 20)), '%');
            } else if (i > 0) {
                partitionAux = (*p)[i - 1];
                if ((partitionAux->part_start + partitionAux->part_size) != partition->part_start) {
                    fprintf(fp,
                            "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#14091A\"><font color=\"#1FD55F\">LIBRE %.2f%c</font></TD>\n",
                            (int) getScale(mbr->mbr_tamanio,
                                           freeSpace((partitionAux->part_start + partitionAux->part_size),
                                                     partition->part_start)), ((getScale(mbr->mbr_tamanio, freeSpace(
                                    (partitionAux->part_start + partitionAux->part_size), partition->part_start)) /
                                                                                20)), '%');
                }
            }


            if (partition->part_type == 'p' || partition->part_type == 'P') {
                fprintf(fp,
                        "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#1FD55F\"><font color=\"#14091A\">%s (PRIMARIA %.2f%c)</font></TD>\n",
                        (int) getScale(mbr->mbr_tamanio, partition->part_size), partition->part_name,
                        ((getScale(mbr->mbr_tamanio, partition->part_size) / 20)), '%');
            } else if (partition->part_type == 'e' || partition->part_type == 'E') {
                fprintf(fp,
                        "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#1FD55F\"><font color=\"#14091A\">%s (EXTENDIDA %.2f%c)</font></TD>\n",
                        (int) getScale(mbr->mbr_tamanio, partition->part_size), partition->part_name,
                        ((getScale(mbr->mbr_tamanio, partition->part_size) / 20)), '%');
            }
            lastIndex = i;
        }

    }


    Partition *partitionAux = (*p)[lastIndex];
    if ((partitionAux->part_start + partitionAux->part_size) != mbr->mbr_tamanio) {
        fprintf(fp,
                "\t\t\t\t\t <TD align=\"center\" width=\"%d\" rowspan=\"2\" height=\"60\" BGCOLOR=\"#14091A\"><font color=\"#1FD55F\">LIBRE %.2f%c</font></TD>\n",
                (int) getScale(mbr->mbr_tamanio,
                               freeSpace((partitionAux->part_start + partitionAux->part_size), mbr->mbr_tamanio)),
                ((getScale(mbr->mbr_tamanio,
                           freeSpace((partitionAux->part_start + partitionAux->part_size), mbr->mbr_tamanio)) / 20)),
                '%');
    }


    fprintf(fp, "\t\t\t\t%s \n", "</TR>");

    fprintf(fp, "\t\t\t%s \n", "</TABLE>");


    fprintf(fp, "\t\t%s \n", ">");
    fprintf(fp, "\t%s \n", "];");
    fprintf(fp, "%s \n", "}");

    fclose(fp);

    char *output = newString(300);
    char *local_file = path;

    char *ts1 = strdup(local_file);
    char *dir = dirname(ts1);

    char *mkDir = newString(100);
    sprintf(mkDir, "mkdir -p \"%s\"/", dir);
    system(mkDir);
    free(mkDir);
    sprintf(output, "dot -Tpng -o %s /home/flores08/Desktop/reps/disk_rep.gv", path);
    system(output);
    free(output);

}

bool graphFileContent(char *id, char *path, char *ruta) {
    int noinode = getFileFolder(id, path);
    inode in;
    getInode(&in, id, noinode);
    bool tiene = 0;
    if (in.i_uid == idUser) {
        if (tienePermiso('r', 'u', in.i_perm))
            tiene = 1;
    } else if (idUser == 1) {
        tiene = 1;
    } else if (in.i_gid == idGrupo) {
        if (tienePermiso('r', 'g', in.i_perm))
            tiene = 1;
    } else {
        if (tienePermiso('r', 'o', in.i_perm))
            tiene = 1;
    }


    if (!tiene) {
        alert(ERR, "El usuario actual no tiene parmisos para leer este archivo");
    } else {
        char *destino = newString(280320);
        char *name = newString(12);

        int folder = getFolder(id, path, FALSE);
        getFileFolderName(name, path);
        getFileContent(id, folder, name, destino);
        printf("# %s\n", destino);
    }
}

double getScale(int sizeDisk, int sizePart) {
    return ((((double) sizePart / (double) sizeDisk) * 100) * 20);
}

int freeSpace(int a, int b) {
    return b - a;
}


void graphDiskContent(char *id, char *path) {
    printf(CYN "##############  C R E A T I N G     T R E E     R E P  ##############\n" RESET);
    printf("%s\n", "");
    //char * path = "/home/flores08/Desktop/reps/DISK.png";
    FILE *fp;
    fp = fopen("/home/flores08/Desktop/reps/files_rep.gv", "wb");
    fprintf(fp, "%s\n", "digraph g {");
    fprintf(fp, "%s\n", "graph[");
    fprintf(fp, "%s\n", "rankdir = \"LR\"");
    fprintf(fp, "%s\n", "bgcolor = \"#0F0F0F\"");
    fprintf(fp, "%s\n", "];");
    fprintf(fp, "%s\n", "node[");
    fprintf(fp, "%s\n", "fontsize = \"16\"");
    fprintf(fp, "%s\n", "fontname = \"Ubuntu Condensed\"");
    fprintf(fp, "%s\n", "fontcolor = \"#FFFFFF\"");
    fprintf(fp, "%s\n", "shape = \"plaintext\"");
    fprintf(fp, "%s\n", "];");
    fprintf(fp, "%s\n", "edge[");
    fprintf(fp, "%s\n", "];");


    inodeString(fp, id, 0);

    fprintf(fp, "%s\n", "}");

    fclose(fp);

    char *output = newString(300);
    char *local_file = path;

    char *ts1 = strdup(local_file);
    char *dir = dirname(ts1);

    char *mkDir = newString(100);
    sprintf(mkDir, "mkdir -p \"%s\"/", dir);
    system(mkDir);
    free(mkDir);
    sprintf(output, "dot -Tpng -o %s /home/flores08/Desktop/reps/files_rep.gv", path);
    system(output);
    free(output);
}

void inodeString(FILE *destino, char *id, int index) {
    inode In;
    getInode(&In, id, index);
    fprintf(destino, "\"inode%d\"[ \n", index);
    fprintf(destino, "%s\n", "label = <");

    fprintf(destino, "<table border=\"1\" cellspacing=\"0\" cellpadding=\"8\">\n", "");
    fprintf(destino, "<tr><td port=\"header\" bgcolor=\"#E3001C\" width=\"250\"><b>I N O D E %d</b></td></tr>\n",
            index);
    fprintf(destino, "<tr><td align=\"left\">i_uid: %d </td></tr>\n", In.i_uid);
    fprintf(destino, "<tr><td align=\"left\">i_gid: %d </td></tr>\n", In.i_gid);
    fprintf(destino, "<tr><td align=\"left\">i_size: %d </td></tr>\n", In.i_size);
    fprintf(destino, "<tr><td align=\"left\">i_perm: %d </td></tr>\n", In.i_perm);
    fprintf(destino, "<tr><td align=\"left\">i_type: %c </td></tr>\n", In.i_type);
    fprintf(destino, "<tr><td align=\"left\">i_atime: %s </td></tr>\n", In.i_atime);
    fprintf(destino, "<tr><td align=\"left\">i_ctime: %s </td></tr>\n", In.i_ctime);
    fprintf(destino, "<tr><td align=\"left\">i_mtime: %s </td></tr>\n", In.i_mtime);
    for (int i = 0; i <= 14; i++) {
        fprintf(destino, "<tr><td align=\"left\" port=\"block%d\">i_block[%d]: %d</td></tr>\n", i, i, In.i_block[i]);
    }
    fprintf(destino, "</table>\n");

    fprintf(destino, "%s\n", ">shape = \"none\"");
    fprintf(destino, "%s\n", "color = white");
    fprintf(destino, "%s\n", "];");

    for (int i = 0; i <= 14; i++) {
        if (i <= 11) {
            if (In.i_block[i] != -1) {
                fprintf(destino, "\"inode%d\":block%d -> \"block%d\":header [\n", index, i, In.i_block[i]);
                fprintf(destino, "color = \"#FFFFFF\"\n");
                fprintf(destino, "];\n");
                bool first = FALSE;
                if (i == 0) {
                    first = TRUE;
                }
                blockString(destino, id, In.i_block[i], In.i_type, first);
            }
        }
    }
}

void blockString(FILE *destino, char *id, int index, char type, bool first) {
    if (type == '0') {
        dBlock dB;
        getFolderBlock(&dB, id, index);
        fprintf(destino, "\"block%d\"[ \n", index);
        fprintf(destino, "%s\n", "label = <");

        fprintf(destino, "<table border=\"1\" cellspacing=\"0\" cellpadding=\"8\">\n", "");
        fprintf(destino, "<tr><td port=\"header\" bgcolor=\"#F7D102\" width=\"250\"><b>B L O C K %d</b></td></tr>\n",
                index);
        for (int i = 0; i <= 3; i++) {
            //fprintf(destino, "<tr><td align=\"left\">b_content[%d].b_name: %s </td></tr>\n", i, dB.b_content[i].b_name);
            fprintf(destino, "<tr><td align=\"left\">b_content[%d].b_name: ", i);
            for (int j = 0; j < 12; j++) {
                if (dB.b_content[i].b_name[j] != '\0') {
                    fprintf(destino, "%c", dB.b_content[i].b_name[j]);
                } else {
                    j = 12;
                }
            }
            fprintf(destino, "</td></tr>\n");
            fprintf(destino, "<tr><td align=\"left\" port=\"inode%d\">b_content[%d].b_inodo: %d </td></tr>\n", i, i,
                    dB.b_content[i].b_inodo);
        }
        fprintf(destino, "</table>\n");

        fprintf(destino, "%s\n", ">shape = \"none\"");
        fprintf(destino, "%s\n", "color = white");
        fprintf(destino, "%s\n", "];");

        int _i = 0;
        if (first) {
            _i = 2;
        }

        for (int i = _i; i <= 3; i++) {
            if (dB.b_content[i].b_inodo != -1) {
                fprintf(destino, "\"block%d\":inode%d -> \"inode%d\":header [\n", index, i, dB.b_content[i].b_inodo);
                fprintf(destino, "color = \"#FFFFFF\"\n");
                fprintf(destino, "];\n");
                inodeString(destino, id, dB.b_content[i].b_inodo);
            }
        }

    } else if (type == '1') {
        fBlock fB;
        getFileBlock(&fB, id, index);
        fprintf(destino, "\"block%d\"[ \n", index);
        fprintf(destino, "%s\n", "label = <");

        fprintf(destino, "<table border=\"1\" cellspacing=\"0\" cellpadding=\"8\">\n", "");
        fprintf(destino, "<tr><td port=\"header\" bgcolor=\"#071596\" width=\"250\"><b>B L O C K %d</b></td></tr>\n",
                index);
        fprintf(destino, "<tr><td align=\"left\">b_content: %.64s</td></tr>\n", fB.b_content);
        /*fprintf(destino, "<tr><td align=\"left\">b_content: ");
        for(int i = 0; i<64; i++){
          if(fB.b_content[i]!='\0'){
            fprintf(destino, "%c", fB.b_content[i]);
          }else{
            i=64;
          }
        }
        fprintf(destino, "</td></tr>\n");*/
        fprintf(destino, "</table>\n");

        fprintf(destino, "%s\n", ">shape = \"none\"");
        fprintf(destino, "%s\n", "color = white");
        fprintf(destino, "%s\n", "];");
    } else if (type == '2') {

    }
}


void graphInodes(char *id, char *path) {
    printf(CYN "#############  C R E A T I N G     I N O D E S     R E P  ############\n\n" RESET);
    //char * path = "/home/flores08/Desktop/reps/INODES.png";
    FILE *fp;
    fp = fopen("/home/flores08/Desktop/reps/inodes_rep.gv", "wb");
    fprintf(fp, "%s\n", "digraph g {");
    fprintf(fp, "%s\n", "graph[");
    fprintf(fp, "%s\n", "rankdir = \"LR\"");
    fprintf(fp, "%s\n", "bgcolor = \"#0F0F0F\"");
    fprintf(fp, "%s\n", "];");
    fprintf(fp, "%s\n", "node[");
    fprintf(fp, "%s\n", "fontsize = \"16\"");
    fprintf(fp, "%s\n", "fontname = \"Ubuntu Condensed\"");
    fprintf(fp, "%s\n", "fontcolor = \"#FFFFFF\"");
    fprintf(fp, "%s\n", "shape = \"plaintext\"");
    fprintf(fp, "%s\n", "];");
    fprintf(fp, "%s\n", "edge[");
    fprintf(fp, "%s\n", "];");

    superBlock sB;
    superBlock *sBpointer;

    getSuperBlock(&sB, id);

    int last = 0;
    for (int i = 0; i < sB.s_inodes_count; i++) {
        char t = getInodeBit(id, i);
        if (t == '1') {
            if (i != 0) {
                fprintf(fp, "\"inode%d\":header -> \"inode%d\":header [\n", last, i);
                fprintf(fp, "color = \"#FFFFFF\"\n");
                fprintf(fp, "];\n");
            }
            inodeListString(fp, id, i);
            last = i;
        }
    }

    fprintf(fp, "%s\n", "}");

    fclose(fp);

    char *output = newString(300);
    char *local_file = path;

    char *ts1 = strdup(local_file);
    char *dir = dirname(ts1);

    char *mkDir = newString(100);
    sprintf(mkDir, "mkdir -p \"%s\"/", dir);
    system(mkDir);
    free(mkDir);
    sprintf(output, "dot -Tpng -o %s /home/flores08/Desktop/reps/inodes_rep.gv", path);
    system(output);
    free(output);
}

void inodeListString(FILE *destino, char *id, int index) {
    inode In;
    getInode(&In, id, index);
    fprintf(destino, "\"inode%d\"[ \n", index);
    fprintf(destino, "%s\n", "label = <");

    fprintf(destino, "<table border=\"1\" cellspacing=\"0\" cellpadding=\"8\">\n", "");
    fprintf(destino, "<tr><td port=\"header\" bgcolor=\"#E3001C\" width=\"250\"><b>I N O D E %d</b></td></tr>\n",
            index);
    fprintf(destino, "<tr><td align=\"left\">i_uid: %d </td></tr>\n", In.i_uid);
    fprintf(destino, "<tr><td align=\"left\">i_gid: %d </td></tr>\n", In.i_gid);
    fprintf(destino, "<tr><td align=\"left\">i_size: %d </td></tr>\n", In.i_size);
    fprintf(destino, "<tr><td align=\"left\">i_perm: %d </td></tr>\n", In.i_perm);
    fprintf(destino, "<tr><td align=\"left\">i_type: %c </td></tr>\n", In.i_type);
    fprintf(destino, "<tr><td align=\"left\">i_atime: %s </td></tr>\n", In.i_atime);
    fprintf(destino, "<tr><td align=\"left\">i_ctime: %s </td></tr>\n", In.i_ctime);
    fprintf(destino, "<tr><td align=\"left\">i_mtime: %s </td></tr>\n", In.i_mtime);
    for (int i = 0; i <= 14; i++) {
        fprintf(destino, "<tr><td align=\"left\" port=\"block%d\">i_block[%d]: %d</td></tr>\n", i, i, In.i_block[i]);
    }
    fprintf(destino, "</table>\n");

    fprintf(destino, "%s\n", ">shape = \"none\"");
    fprintf(destino, "%s\n", "color = white");
    fprintf(destino, "%s\n", "];");
}
