#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/62ecc083/edit.o \
	${OBJECTDIR}/_ext/62ecc083/mkUsr.o \
	${OBJECTDIR}/_ext/62ecc083/ren.o \
	${OBJECTDIR}/_ext/62ecc083/rmUsr.o \
	${OBJECTDIR}/binaryFiles.o \
	${OBJECTDIR}/commands.o \
	${OBJECTDIR}/disk.o \
	${OBJECTDIR}/fDisk.o \
	${OBJECTDIR}/list.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/mkDir.o \
	${OBJECTDIR}/mkDisk.o \
	${OBJECTDIR}/mkFile.o \
	${OBJECTDIR}/mkFs.o \
	${OBJECTDIR}/mkGrp.o \
	${OBJECTDIR}/mount.o \
	${OBJECTDIR}/rep.o \
	${OBJECTDIR}/rmDisk.o \
	${OBJECTDIR}/structures.o \
	${OBJECTDIR}/unMount.o \
	${OBJECTDIR}/utils.o \
	${OBJECTDIR}/utils_gpvz.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/62ecc083/edit.o: ../../actualizado/MIA/edit.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/62ecc083
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/62ecc083/edit.o ../../actualizado/MIA/edit.c

${OBJECTDIR}/_ext/62ecc083/mkUsr.o: ../../actualizado/MIA/mkUsr.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/62ecc083
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/62ecc083/mkUsr.o ../../actualizado/MIA/mkUsr.c

${OBJECTDIR}/_ext/62ecc083/ren.o: ../../actualizado/MIA/ren.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/62ecc083
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/62ecc083/ren.o ../../actualizado/MIA/ren.c

${OBJECTDIR}/_ext/62ecc083/rmUsr.o: ../../actualizado/MIA/rmUsr.c 
	${MKDIR} -p ${OBJECTDIR}/_ext/62ecc083
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/62ecc083/rmUsr.o ../../actualizado/MIA/rmUsr.c

${OBJECTDIR}/binaryFiles.o: binaryFiles.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/binaryFiles.o binaryFiles.c

${OBJECTDIR}/commands.o: commands.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/commands.o commands.c

${OBJECTDIR}/disk.o: disk.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/disk.o disk.c

${OBJECTDIR}/fDisk.o: fDisk.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/fDisk.o fDisk.c

${OBJECTDIR}/list.o: list.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/list.o list.c

${OBJECTDIR}/main.o: main.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.c

${OBJECTDIR}/mkDir.o: mkDir.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mkDir.o mkDir.c

${OBJECTDIR}/mkDisk.o: mkDisk.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mkDisk.o mkDisk.c

${OBJECTDIR}/mkFile.o: mkFile.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mkFile.o mkFile.c

${OBJECTDIR}/mkFs.o: mkFs.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mkFs.o mkFs.c

${OBJECTDIR}/mkGrp.o: mkGrp.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mkGrp.o mkGrp.c

${OBJECTDIR}/mount.o: mount.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/mount.o mount.c

${OBJECTDIR}/rep.o: rep.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rep.o rep.c

${OBJECTDIR}/rmDisk.o: rmDisk.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/rmDisk.o rmDisk.c

${OBJECTDIR}/structures.o: structures.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/structures.o structures.c

${OBJECTDIR}/unMount.o: unMount.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/unMount.o unMount.c

${OBJECTDIR}/utils.o: utils.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils.o utils.c

${OBJECTDIR}/utils_gpvz.o: utils_gpvz.c 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/utils_gpvz.o utils_gpvz.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/mia

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
