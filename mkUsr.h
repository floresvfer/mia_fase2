#ifndef MKUSR_H
#define MKUSR_H

#include "utils.h"

struct mkusr {
    char *usr;
    char *pwd;
    char *grp;
    bool valid;
} typedef mkUsr;

mkUsr *newMkUsr(char *usr, char *pwd, char *grp, bool valid);

#endif /* MKUSR_H */
