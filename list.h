#ifndef LIST_H_INCLUDED
#define LIST_H_INCLUDED

#include "disk.h"

typedef struct list {
    disk *head;
} list;

void insert_front(list *_list, char *id, char *path, char *name);

void insert_disk_top(list *_list, disk *_disk);

void delete_front(list *_list);

void insert_back(list *_list, char *id, char *path, char *name);

void delete_back(list *_list);

void remove_any(list *_list, char *id);

disk *search(disk *head, disk *cursor, char *id);

disk *search_name(disk *head, disk *cursor, char *name, char *path);

void print(list *_list);

void dispose(list *_list);


typedef struct list_n {
    node *head;
} list_n;


void insert_front_n(list_n *_list, char *path, char letra, int count);

void insert_disk_top_n(list_n *_list, node *_disk);

void delete_front_n(list_n *_list);

void insert_back_n(list_n *_list, char *path, char letra, int count);

void delete_back_n(list_n *_list);

void remove_any_n(list_n *_list, char letra);

node *search_n(node *head, node *cursor, char letra);

node *search_n_path(node *head, node *cursor, char *path);

void print_n(list_n *_list);

void dispose_n(list_n *_list);


#endif // LIST_H_INCLUDED
