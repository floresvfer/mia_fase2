#ifndef MIA_REM_H
#define MIA_REM_H

#include "structures.h"
#include "utils.h"

typedef struct rem {
    char *path;
    bool valid;
} Rem;

Rem *newRem(char *path, bool valid);

#endif //MIA_REM_H
