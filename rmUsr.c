#include "rmUsr.h"

rmUsr *newRmUsr(char *usr, bool valid) {
    rmUsr *tmp = malloc(sizeof(rmUsr));

    tmp->usr = newString(10);
    tmp->valid = valid;

    tmp->usr = usr;

    if (strlen(usr) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -usr OBLIGATORIO");
    }

    return tmp;
}
