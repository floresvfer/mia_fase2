#ifndef STRUCTURES_H_INCLUDED
#define STRUCTURES_H_INCLUDED

#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

extern const char PARTITION_STATUS_NOT_ACTIVE;
extern const char PARTITION_STATUS_ACTIVE;
extern const char PARTITION_TYPE_PRIMARY;
extern const char PARTITION_TYPE_EXTENDED;


typedef struct partition {
    char part_status;
    char part_type;
    char part_fit[2];
    int part_start;
    int part_size;
    char part_name[16];
} Partition;

typedef struct mbr {
    int mbr_tamanio;
    char mbr_fecha_creacion[16]; // dd/mm/yyyy hh:ss
    int mbr_disk_signature;
    Partition mbr_partition[4];
} Mbr;

typedef struct ebr {
    char part_status;
    char part_fit[2];
    int part_start;
    int part_size;
    int part_next;
    char part_name[16];
} Ebr;

typedef struct superblock {
    int s_filesystem_type;//numero que identifica el sistema de archivos
    int s_inodes_count;   //numero total de inodos
    int s_blocks_count;   //numero total de bloques
    int s_free_inodes_count; //numero de inodos libre
    int s_free_blocks_count; //numero de bloques libres
    char s_mtime[16];     //ultima fecha en que el sistema fue montado
    char s_umtime[16];    //ulima fecha en que el sistema fue desmontado
    int s_mnt_count;      //numero de veces que el sistema ha sido montado
    int s_magic;          // 0xEF53
    int s_inode_size;     //tamaño del inodo
    int s_block_size;     //tamaño del bloque
    int s_first_ino;      //primer inodo libre
    int s_first_blo;      //primer bloque libre
    int s_bm_inode_start; //inicio del bitmap de inodos
    int s_bm_block_start; //inicio del bitmap de bloques
    int s_inode_start;    //inicio de la tabla de inodos
    int s_block_start;    //inicio de la tabla de bloques
} superBlock;

typedef struct inode {
    int i_uid;
    int i_gid;
    int i_size;
    char i_atime[16];
    char i_ctime[16];
    char i_mtime[16];
    int i_block[15];
    char i_type;
    int i_perm;
} inode;


typedef struct content {
    char b_name[12];
    int b_inodo;
} content;

typedef struct dblock {
    content b_content[4];
} dBlock;


typedef struct fblock {
    char b_content[64];
} fBlock;


typedef struct pblock {
    int b_pointer[16];
} pBlock;


typedef struct journal {
    char *journal_tipo_operacion;
    int journal_tipo;
    char journal_nombre[12];
    char journal_contenido[200];
    char journal_fecha[16];
    int journal_propietario;
    int journal_permisos;
} journal;


typedef struct jjjj {
    int typo_OP;
    int typo;
    char cont[200];
    char fecha[16];
    char path1[70];
    char path2[70];
    int propietario;
    int permisos;
}
        Jjjj;


void configEmptyPartition(Partition *part);

void configEmptyEbr(Ebr *ebr);

void configEmptyPartitionFast(Partition *part);

void configPartitionAddMinBack(Partition *part, int m);

void configPartitionAddMinFront(Partition *part, int m);

void configPartition(Partition *part, char status, char type, char *fit, int start, int size, char *name);

void configEbr(Ebr *ebr, char status, char fit, int start, int size, int next, char *name);

void sortPartitions(Mbr *mbr);

void compressPartitions(Mbr *mbr);

void getDate(char *destiny);

int structuresAmountEXT2(int part_size);

int structuresAmountEXT3(int part_size);

void configSuperBlock(int part_size, int part_start, superBlock *superBloque, int s_filesystem_type);

void configInode(inode *In, int i_uid, int i_gid, int i_size, char i_type, int i_perm);

void configFirstFolderBlock(dBlock *Dblock, int padre, int me);

void configFolderBlock(dBlock *Dblock);

void configFileBlock(fBlock *Fblock, char *content);

void configPointerBlock(pBlock *Pblock);

int getInodeTablePos(superBlock *sB, int index);

int getBlockTablePos(superBlock *sB, int index);

int getInodeBitMapPos(superBlock *sB, int index);

int getBlockBitMapPos(superBlock *sB, int index);

int getInodeCount(superBlock *sB);

int getBlockCount(superBlock *sB);

int getInodeFreeCount(superBlock *sB);

int getBlockFreeCount(superBlock *sB);


int searchFreePointerIntoInode(inode *In);


#endif // STRUCTURES_H_INCLUDED
