#ifndef EDIT_H
#define EDIT_H

#include "utils.h"

struct edit {
    char *path;
    int size;
    char *cont;
    bool valid;
} typedef Edit;

Edit *newEdit(char *path, int size, char *cont, bool valid);

#endif /* EDIT_H */
