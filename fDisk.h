#ifndef FDISK_H_INCLUDED
#define FDISK_H_INCLUDED

#include "utils.h"

struct fdisk {
    int size;
    char unit;
    char *path;
    char type;
    char *fit;
    char *del;
    char *name;
    int add;
    bool valid;
} typedef fDisk;

fDisk *newFDisk(int size, char unit, char *path, char type, char *fit, char *del, char *name, int add, bool valid);

#endif // FDISK_H_INCLUDED
