#include "mkUsr.h"

mkUsr *newMkUsr(char *usr, char *pwd, char *grp, bool valid) {
    mkUsr *tmp = malloc(sizeof(mkUsr));

    tmp->usr = newString(10);
    tmp->pwd = newString(10);
    tmp->grp = newString(10);
    tmp->valid = valid;

    tmp->usr = usr;
    tmp->pwd = pwd;
    tmp->grp = grp;

    if (strlen(usr) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -usr OBLIGATORIO");
    }

    if (strlen(pwd) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -pwd OBLIGATORIO");
    }

    if (strlen(grp) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -grp OBLIGATORIO");
    }

    return tmp;
}
