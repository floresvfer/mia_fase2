#include "structures.h"

const char PARTITION_STATUS_NOT_ACTIVE = 'F';
const char PARTITION_STATUS_ACTIVE = 'T';
const char PARTITION_TYPE_PRIMARY = 'P';
const char PARTITION_TYPE_EXTENDED = 'E';

void configEmptyPartition(Partition *part) {
    part->part_status = PARTITION_STATUS_NOT_ACTIVE;
    part->part_type = '\0';
    strncpy(part->part_fit, "\0\0", 2);
    part->part_start = 0;
    part->part_size = 0;
    strncpy(part->part_name, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 15);
}

void configEmptyEbr(Ebr *ebr) {
    strncpy(ebr->part_fit, "\0\0", 2);
    ebr->part_next = 0;
    ebr->part_size = 0;
    ebr->part_start = 0;
    ebr->part_status = '\0';
    strncpy(ebr->part_name, "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0", 15);
}


void configEmptyPartitionFast(Partition *part) {
    part->part_status = PARTITION_STATUS_NOT_ACTIVE;
}

void configPartitionAddMinBack(Partition *part, int m) {
    part->part_size = part->part_size + m;
}

void configPartitionAddMinFront(Partition *part, int m) {
    part->part_start = part->part_start - m;
}

void configPartition(Partition *part, char status, char type, char *fit, int start, int size, char *name) {
    part->part_status = status;
    part->part_type = type;
    strncpy(part->part_fit, fit, 2);
    part->part_start = start;
    part->part_size = size;
    strncpy(part->part_name, name, 15);
}

void configEbr(Ebr *ebr, char status, char fit, int start, int size, int next, char *name) {
    ebr->part_status = status;
    strncpy(ebr->part_fit, fit, 2);
    ebr->part_start = start;
    ebr->part_size = size;
    ebr->part_next = next;
    strncpy(ebr->part_name, name, 15);
}

void sortPartitions(Mbr *mbr) {
    Partition *array[4];
    for (int i = 0; i < 4; i++) {
        array[i] = &(mbr->mbr_partition[i]);
    }

    for (int i = 0; i < 4; i++) {
        for (int j = 1; j < 4; j++) {
            if (array[j - 1]->part_start > array[j]->part_start) {

                Partition *tmp = (Partition *) malloc(sizeof(Partition));

                configPartition(tmp,
                                array[j - 1]->part_status,
                                array[j - 1]->part_type,
                                array[j - 1]->part_fit,
                                array[j - 1]->part_start,
                                array[j - 1]->part_size,
                                array[j - 1]->part_name);
                configPartition(&(mbr->mbr_partition[j - 1]),
                                array[j]->part_status,
                                array[j]->part_type,
                                array[j]->part_fit,
                                array[j]->part_start,
                                array[j]->part_size,
                                array[j]->part_name);
                configPartition(&(mbr->mbr_partition[j]),
                                tmp->part_status,
                                tmp->part_type,
                                tmp->part_fit,
                                tmp->part_start,
                                tmp->part_size,
                                tmp->part_name);
                free(tmp);
            }
        }
    }
    compressPartitions(mbr);
}

void compressPartitions(Mbr *mbr) {
    Partition *array[4];
    for (int i = 0; i < 4; i++) {
        array[i] = &(mbr->mbr_partition[i]);
    }

    for (int i = 0; i < 4; i++) {
        for (int j = 1; j < 4; j++) {
            if (array[j - 1]->part_status < array[j]->part_status) {

                Partition *tmp = (Partition *) malloc(sizeof(Partition));

                configPartition(tmp,
                                array[j - 1]->part_status,
                                array[j - 1]->part_type,
                                array[j - 1]->part_fit,
                                array[j - 1]->part_start,
                                array[j - 1]->part_size,
                                array[j - 1]->part_name);
                configPartition(&(mbr->mbr_partition[j - 1]),
                                array[j]->part_status,
                                array[j]->part_type,
                                array[j]->part_fit,
                                array[j]->part_start,
                                array[j]->part_size,
                                array[j]->part_name);
                configPartition(&(mbr->mbr_partition[j]),
                                tmp->part_status,
                                tmp->part_type,
                                tmp->part_fit,
                                tmp->part_start,
                                tmp->part_size,
                                tmp->part_name);

                free(tmp);
            }
        }
    }
}


void getDate(char *destiny) {
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    if (tm.tm_mday < 10 && (tm.tm_mon + 1) < 10) {
        sprintf(destiny, "%c%d%c%d%d %d:%d", '0', tm.tm_mday, '0', tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour,
                tm.tm_sec);
    } else if (tm.tm_mday < 10 && tm.tm_mon > 10) {
        sprintf(destiny, "%c%d%d%d %d:%d", '0', tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_sec);
    } else if (tm.tm_mday > 10 && tm.tm_mon < 10) {
        sprintf(destiny, "%d%c%d%d %d:%d", tm.tm_mday, '0', tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_sec);
    } else {
        sprintf(destiny, "%d%d%d %d:%d", tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_sec);
    }
}

int structuresAmountEXT2(int part_size) {
    return (int) (
            (part_size - (int) sizeof(superBlock))
            /
            (1 + 3 + (int) sizeof(inode) + (3 * (int) sizeof(fBlock)))
    );
}

int structuresAmountEXT3(int part_size) {
    return (int) (
            (part_size - (int) sizeof(superBlock))
            /
            (1 + (int) sizeof(journal) + 3 + (int) sizeof(inode) + (3 * (int) sizeof(fBlock)))
    );
}

void configSuperBlock(int part_size, int part_start, superBlock *superBloque, int s_filesystem_type) {
    int n = 0;
    if (s_filesystem_type == 2) {
        n = structuresAmountEXT2(part_size);
    } else if (s_filesystem_type == 3) {
        n = structuresAmountEXT3(part_size);
    }
    superBloque->s_filesystem_type = s_filesystem_type;
    superBloque->s_inodes_count = n;
    superBloque->s_blocks_count = n * 3;
    superBloque->s_free_inodes_count = n;
    superBloque->s_free_blocks_count = n * 3;
    getDate(superBloque->s_mtime);
    getDate(superBloque->s_umtime);
    superBloque->s_mnt_count = 1;
    superBloque->s_magic = 61267;
    superBloque->s_inode_size = (int) sizeof(inode);
    superBloque->s_block_size = (int) sizeof(fBlock);
    superBloque->s_first_ino = 0;
    superBloque->s_first_blo = 0;
    if (s_filesystem_type == 2) {
        superBloque->s_bm_inode_start = part_start + (int) sizeof(superBlock);
    } else if (s_filesystem_type == 3) {
        superBloque->s_bm_inode_start = part_start + (int) sizeof(superBlock) + (n * (int) sizeof(journal));
    }
    superBloque->s_bm_block_start = superBloque->s_bm_inode_start + n;
    superBloque->s_inode_start = superBloque->s_bm_block_start + (3 * n);
    superBloque->s_block_start = superBloque->s_inode_start + (n * (int) sizeof(inode));
}

void configInode(inode *In, int i_uid, int i_gid, int i_size, char i_type, int i_perm) {
    In->i_size = i_size;
    In->i_uid = i_uid;
    In->i_gid = i_gid;
    In->i_type = i_type;
    In->i_perm = i_perm;
    for (int i = 0; i < 15; i++) {
        In->i_block[i] = -1;
    }
    getDate(In->i_atime);
    getDate(In->i_ctime);
    getDate(In->i_mtime);
}

/*void configFirstFolderBlock(content * cont, char * name, int pointer){

}*/
void configFileBlock(fBlock *Fblock, char *content) {
    sprintf(Fblock->b_content, "%.64s", content);
    for (int i = strlen(content); i < 64; i++) {
        Fblock->b_content[i] = '\0';
    }
}

void configFirstFolderBlock(dBlock *Dblock, int padre, int me) {
    sprintf(Dblock->b_content[0].b_name, "%s", ".");
    Dblock->b_content[0].b_inodo = me;
    for (int i = strlen("."); i < 12; i++) {
        Dblock->b_content[0].b_name[i] = '\0';
    }

    sprintf(Dblock->b_content[1].b_name, "%s", "..");
    Dblock->b_content[1].b_inodo = padre;
    for (int i = strlen(".."); i < 12; i++) {
        Dblock->b_content[1].b_name[i] = '\0';
    }

    sprintf(Dblock->b_content[2].b_name, "%s", "\0\0\0\0\0\0\0\0\0\0\0\0");
    Dblock->b_content[2].b_inodo = -1;

    sprintf(Dblock->b_content[3].b_name, "%s", "\0\0\0\0\0\0\0\0\0\0\0\0");
    Dblock->b_content[3].b_inodo = -1;
}

void configFolderBlock(dBlock *Dblock) {
    for (int i = 0; i < 4; i++) {
        sprintf(Dblock->b_content[i].b_name, "%s", "\0\0\0\0\0\0\0\0\0\0\0\0");
        Dblock->b_content[i].b_inodo = -1;
    }
}

void configPointerBlock(pBlock *Pblock) {
    for (int i = 0; i < 16; i++) {
        Pblock->b_pointer[i] = -1;
    }
}

int getInodeTablePos(superBlock *sB, int index) {
    return sB->s_inode_start + (index * (int) sizeof(inode));
}

int getBlockTablePos(superBlock *sB, int index) {
    return sB->s_block_start + (index * (int) sizeof(fBlock));
}

int getInodeBitMapPos(superBlock *sB, int index) {
    return sB->s_bm_inode_start + index;
}

int getBlockBitMapPos(superBlock *sB, int index) {
    return sB->s_bm_block_start + index;
}

int getInodeCount(superBlock *sB) {
    return sB->s_inodes_count;
}

int getBlockCount(superBlock *sB) {
    return sB->s_blocks_count;
}

int getInodeFreeCount(superBlock *sB) {
    return sB->s_free_inodes_count;
}

int getBlockFreeCount(superBlock *sB) {
    return sB->s_free_blocks_count;
}
