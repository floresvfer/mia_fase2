#ifndef MIA_CHMOD_H
#define MIA_CHMOD_H

#include "structures.h"
#include "utils.h"

typedef struct chmod {
    char *path;
    bool recursivo;
    bool valid;
    int ugo;
} Chmod;

Chmod *newChmod(char *path, int ugo, bool recursivo, bool valid);

#endif //MIA_CHMOD_H
