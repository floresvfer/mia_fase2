#include "commands.h"

int getSession() {
    if (idUser != -1)
        return 1;
    alert(ERR, "No existe una sesion actualmente");
    return 0;
}

int getCommand(char *command) {

    char *tmp = newString(10);
    int index = 0;
    if (command[index] != '#') {
        while (command[index] != ' ' && index < strlen(command)) {
            tmp[index] = command[index];
            index++;
        }
        index++;

        switch (commandType(tmp)) {
            case 0:
                alert(TRM, "mkdisk");
                command_mkdisk(command, index);
                break;

            case 1:
                alert(TRM, "rmdisk");
                command_rmdisk(command, index);
                break;

            case 2:
                alert(TRM, "fdisk");
                command_fdisk(command, index);
                break;

            case 3:
                alert(TRM, "mount");
                command_mount(command, index);
                break;

            case 4:
                alert(TRM, "unmount");
                command_unmount(command, index);
                break;

            case 5:
                alert(TRM, "exec");
                command_exec(command, index);
                break;

            case 6:
                alert(TRM, "rep");
                command_rep(command, index);
                break;

            case 7:
                alert(TRM, "mkfs");
                command_mkfs(command, index);
                break;

            case 8:
                alert(TRM, "mkfile");
                if (getSession())
                    command_mkfile(command, index);
                break;

            case 9:
                alert(TRM, "mkgrp");
                if (getSession())
                    command_mkgrp(command, index);
                break;

            case 10:
                alert(TRM, "rmgrp");
                if (getSession())
                    command_rmgrp(command, index);
                break;

            case 11:
                alert(TRM, "mkusr");
                if (getSession())
                    command_mkusr(command, index);
                break;

            case 12:
                alert(TRM, "rmusr");
                if (getSession())
                    command_rmusr(command, index);
                break;

            case 13:
                alert(TRM, "edit");
                if (getSession())
                    command_edit(command, index);
                break;

            case 14:
                alert(TRM, "ren");
                if (getSession())
                    command_ren(command, index);
                break;

            case 15:
                alert(TRM, "mkdir");
                if (getSession())
                    command_mkdir(command, index);
                break;

            case 16:
                alert(TRM, "cat");
                if (getSession())
                    command_cat(command, index);
                break;

            case 17:
                alert(TRM, "chmod");
                if (getSession())
                    command_chmod(command, index);
                break;

            case 18:
                alert(TRM, "login");
                command_login(command, index);
                break;

            case 19:
                alert(TRM, "logout");
                command_logout(command, index);
                break;

            case 20:
                alert(TRM, "cp");
                if (getSession())
                    command_cp(command, index);
                break;

            case 21:
                alert(TRM, "chown");
                if (getSession())
                    command_chown(command, index);
                break;

            case 22:
                alert(TRM, "rem");
                if (getSession())
                    command_rem(command, index);
                break;

            case 23:
                alert(TRM, "mv");
                if (getSession())
                    command_mv(command, index);
                break;

            case 24:
                alert(TRM, "loss");
                command_loss(command, index);
                break;

            case 25:
                alert(TRM, "recovery");
                command_recover(command, index);
                break;

            default:
                alert(ERR, "comando no reconocido");
                break;
        }
    }
    free(tmp);
}

int commandType(char *name) {
    if (stricmp(name, "mkdisk") == 0)
        return 0;

    if (stricmp(name, "rmdisk") == 0)
        return 1;

    if (stricmp(name, "fdisk") == 0)
        return 2;

    if (stricmp(name, "mount") == 0)
        return 3;

    if (stricmp(name, "unmount") == 0)
        return 4;

    if (stricmp(name, "exec") == 0)
        return 5;

    if (stricmp(name, "rep") == 0)
        return 6;

    if (stricmp(name, "mkfs") == 0)
        return 7;

    if (stricmp(name, "mkfile") == 0)
        return 8;

    if (stricmp(name, "mkgrp") == 0)
        return 9;

    if (stricmp(name, "rmgrp") == 0)
        return 10;

    if (stricmp(name, "mkusr") == 0)
        return 11;

    if (stricmp(name, "rmusr") == 0)
        return 12;

    if (stricmp(name, "edit") == 0)
        return 13;

    if (stricmp(name, "ren") == 0)
        return 14;

    if (stricmp(name, "mkdir") == 0)
        return 15;

    if (stricmp(name, "cat") == 0)
        return 16;

    if (stricmp(name, "chmod") == 0)
        return 17;

    if (stricmp(name, "login") == 0)
        return 18;

    if (stricmp(name, "logout") == 0)
        return 19;

    if (stricmp(name, "cp") == 0)
        return 20;

    if (stricmp(name, "chown") == 0)
        return 21;

    if (stricmp(name, "rem") == 0)
        return 22;

    if (stricmp(name, "mv") == 0)
        return 23;

    if (stricmp(name, "loss") == 0)
        return 24;

    if (stricmp(name, "recovery") == 0)
        return 25;

    return -1;
}

void command_login(char *command, int index) {
    char *usr = "";
    char *pwd = "";
    char *id = "";
    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "usr") == 0 ||
                    stricmp(type, "pwd") == 0 ||
                    stricmp(type, "id") == 0) {

                    if (stricmp(type, "usr") == 0) {
                        usr = arg;
                    }

                    if (stricmp(type, "pwd") == 0) {
                        pwd = arg;
                    }

                    if (stricmp(type, "id") == 0) {
                        id = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }


    ingresar(newLogIn(usr, pwd, id, valid));
}

void command_mkdisk(char *command, int index) {

    char *path = "";
    char *unit = "";
    int size = -1;

    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

//    while(command[index]!= '-' && index < strlen(command)){
//      type[index_type] = command[index];
//      index++;
//      index_type++;
//    }

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "unit") == 0 ||
                    stricmp(type, "fit") == 0 ||
                    stricmp(type, "size") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "unit") == 0) {
                        if (strlen(arg) == 1) {
                            unit = arg[0];
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN CARACTER");
                        }

                    }

                    if (stricmp(type, "size") == 0) {
                        if (isNumber(arg)) {
                            size = atoi(arg);
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
                        }
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }


    newDisk(newMkDisk(path, unit, size, valid));

}

void command_rmdisk(char *command, int index) {
    char *path = "";

    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0) {
                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    delDisk(newRmDisk(path, valid));

}

void command_fdisk(char *command, int index) {
    int size = -1;
    char unit = ' ';
    char *path = "";
    char typep = ' ';
    char *fit = "";
    char *del = "";
    char *name = "";
    int add = 0;
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "unit") == 0 ||
                    stricmp(type, "size") == 0 ||
                    stricmp(type, "type") == 0 ||
                    stricmp(type, "fit") == 0 ||
                    stricmp(type, "delete") == 0 ||
                    stricmp(type, "name") == 0 ||
                    stricmp(type, "add") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "unit") == 0) {
                        if (strlen(arg) == 1) {
                            unit = arg[0];
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN CARACTER");
                        }
                    }

                    if (stricmp(type, "size") == 0) {
                        if (isNumber(arg)) {
                            size = atoi(arg);
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
                        }
                    }

                    if (stricmp(type, "type") == 0) {
                        if (strlen(arg) == 1) {
                            typep = arg[0];
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN CARACTER");
                        }
                    }

                    if (stricmp(type, "fit") == 0) {
                        fit = arg;
                    }

                    if (stricmp(type, "delete") == 0) {
                        del = arg;
                    }

                    if (stricmp(type, "name") == 0) {
                        name = arg;
                    }

                    if (stricmp(type, "add") == 0) {
                        if (isNumber(arg)) {
                            add = atoi(arg);
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
                        }
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    newPartition(newFDisk(size, unit, path, typep, fit, del, name, add, valid));
}

void command_mount(char *command, int index) {
    char *path;
    char *name;

    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':' && index < strlen(command)) {
            index++;
            if (command[index] == '~' && index < strlen(command)) {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "name") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "name") == 0) {
                        name = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    mountPartition(newMount(path, name, valid));

}

void command_unmount(char *command, int index) {
    char *id = "";

    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "id") == 0) {

                    if (stricmp(type, "id") == 0) {
                        id = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    unMountPartition(newUnMount(id, valid));
}

void command_mkfs(char *command, int index) {

    char *id = "";
    char *tipe = "";
    char *fs = "";
    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':' && index < strlen(command)) {
            index++;
            if (command[index] == '~' && index < strlen(command)) {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "fs") == 0 ||
                    stricmp(type, "id") == 0 ||
                    stricmp(type, "type") == 0) {

                    if (stricmp(type, "fs") == 0) {
                        fs = arg;
                    }

                    if (stricmp(type, "id") == 0) {
                        id = arg;
                    }

                    if (stricmp(type, "type") == 0) {
                        tipe = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                    break;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    mkFsPartition(newMkFs(id, tipe, fs, valid));
}

void command_mkdir(char *command, int index) {
    char *path = "";
    bool valid = TRUE;
    bool padre = FALSE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        if ((command[index] == 'p' && command[index + 1] == ' ') ||
            (command[index] == 'p' && index + 1 == strlen(command))) {
            padre = TRUE;
            index++;
            index++;
        } else {
            while (command[index] != '~' && index < strlen(command)) {
                type[index_type] = command[index];
                index++;
                index_type++;
            }
            index++;
            if (command[index] == ':') {
                index++;
                if (command[index] == '~') {
                    index++;
                    if (command[index] == '"') {
                        index++;
                        while (command[index] != '"' && index < strlen(command)) {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                        index++;
                    } else {
                        while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                    }
                    index++;

                    if (stricmp(type, "path") == 0) {

                        if (stricmp(type, "path") == 0) {
                            path = arg;
                        }

                    } else {
                        alert(ERR, "no se reconoce el parametro especificado");
                        valid = FALSE;
                    }
                } else {
                    alert(ERR, "se esperaba '~' ");
                    valid = FALSE;
                    break;
                }
            } else {
                alert(ERR, "se esperaba ':' ");
                valid = FALSE;
                break;
            }

        }
    }
    makeFolder(newMkDir("", path, padre, valid));
}

void command_mkfile(char *command, int index) {
    int size = 0;
    char *path = "";
    char *cont = "";
    bool valid = TRUE;
    bool padre = FALSE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        if ((command[index] == 'p' && command[index + 1] == ' ') ||
            (command[index] == 'p' && index + 1 == strlen(command))) {
            padre = TRUE;
            index++;
            index++;
        } else {
            while (command[index] != '~' && index < strlen(command)) {
                type[index_type] = command[index];
                index++;
                index_type++;
            }
            index++;
            if (command[index] == ':') {
                index++;
                if (command[index] == '~') {
                    index++;
                    if (command[index] == '"') {
                        index++;
                        while (command[index] != '"' && index < strlen(command)) {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                        index++;
                    } else {
                        while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                    }
                    index++;

                    if (stricmp(type, "path") == 0 ||
                        stricmp(type, "size") == 0 ||
                        stricmp(type, "cont") == 0) {

                        if (stricmp(type, "path") == 0) {
                            path = arg;
                        }

                        if (stricmp(type, "size") == 0) {
                            if (isNumber(arg)) {
                                size = atoi(arg);
                            } else {
                                alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
                            }
                        }

                        if (stricmp(type, "cont") == 0) {
                            cont = arg;
                        }

                    } else {
                        alert(ERR, "no se reconoce el parametro especificado");
                        valid = FALSE;
                    }
                } else {
                    alert(ERR, "se esperaba '~' ");
                    valid = FALSE;
                    break;
                }
            } else {
                alert(ERR, "se esperaba ':' ");
                valid = FALSE;
                break;
            }

        }
    }
    makeFile(newMkFile(path, "", size, cont, padre, valid));
}

void command_rem(char *command, int index) {
    char *path = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;


        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "size") == 0 ||
                    stricmp(type, "cont") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    removeFile_Folder(newRem(path, valid));
}

void command_chmod(char *command, int index) {
    int ugo = -1;
    char *path = "";
    bool valid = TRUE;
    bool recursivo = FALSE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        if ((command[index] == 'r' && command[index + 1] == ' ') ||
            (command[index] == 'r' && index + 1 == strlen(command))) {
            recursivo = TRUE;
            index++;
            index++;
        } else {
            while (command[index] != '~' && index < strlen(command)) {
                type[index_type] = command[index];
                index++;
                index_type++;
            }
            index++;
            if (command[index] == ':') {
                index++;
                if (command[index] == '~') {
                    index++;
                    if (command[index] == '"') {
                        index++;
                        while (command[index] != '"' && index < strlen(command)) {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                        index++;
                    } else {
                        while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                    }
                    index++;

                    if (stricmp(type, "path") == 0 ||
                        stricmp(type, "ugo") == 0) {

                        if (stricmp(type, "path") == 0) {
                            path = arg;
                        }

                        if (stricmp(type, "ugo") == 0) {
                            if (isNumber(arg)) {
                                ugo = atoi(arg);
                            } else {
                                alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
                            }
                        }

                    } else {
                        alert(ERR, "no se reconoce el parametro especificado");
                        valid = FALSE;
                    }
                } else {
                    alert(ERR, "se esperaba '~' ");
                    valid = FALSE;
                    break;
                }
            } else {
                alert(ERR, "se esperaba ':' ");
                valid = FALSE;
                break;
            }

        }
    }
    chMod(newChmod(path, ugo, recursivo, valid));
}

void command_chown(char *command, int index) {
    char *usr = "";
    char *path = "";
    bool valid = TRUE;
    bool recursivo = FALSE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        if ((command[index] == 'r' && command[index + 1] == ' ') ||
            (command[index] == 'r' && index + 1 == strlen(command))) {
            recursivo = TRUE;
            index++;
            index++;
        } else {
            while (command[index] != '~' && index < strlen(command)) {
                type[index_type] = command[index];
                index++;
                index_type++;
            }
            index++;
            if (command[index] == ':') {
                index++;
                if (command[index] == '~') {
                    index++;
                    if (command[index] == '"') {
                        index++;
                        while (command[index] != '"' && index < strlen(command)) {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                        index++;
                    } else {
                        while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                            arg[index_arg] = command[index];
                            index++;
                            index_arg++;
                        }
                    }
                    index++;

                    if (stricmp(type, "path") == 0 ||
                        stricmp(type, "usr") == 0) {

                        if (stricmp(type, "path") == 0) {
                            path = arg;
                        }

                        if (stricmp(type, "usr") == 0) {
                            usr = arg;
                        }

                    } else {
                        alert(ERR, "no se reconoce el parametro especificado");
                        valid = FALSE;
                    }
                } else {
                    alert(ERR, "se esperaba '~' ");
                    valid = FALSE;
                    break;
                }
            } else {
                alert(ERR, "se esperaba ':' ");
                valid = FALSE;
                break;
            }

        }
    }
    chOwn(newChown(path, usr, recursivo, valid));
}

void command_mkgrp(char *command, int index) {
    char *name = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "name") == 0) {

                    if (stricmp(type, "name") == 0) {
                        name = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    makeGroup(newMkGrp(name, valid));
}

void command_rmgrp(char *command, int index) {
    char *name = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "name") == 0) {

                    if (stricmp(type, "name") == 0) {
                        name = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    remGroup(newMkGrp(name, valid));
}

void command_mkusr(char *command, int index) {
    char *usr = "";
    char *pwd = "";
    char *grp = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "usr") == 0 ||
                    stricmp(type, "pwd") == 0 ||
                    stricmp(type, "grp") == 0) {

                    if (stricmp(type, "usr") == 0) {
                        usr = arg;
                    }

                    if (stricmp(type, "pwd") == 0) {
                        pwd = arg;
                    }

                    if (stricmp(type, "grp") == 0) {
                        grp = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    makeUser(newMkUsr(usr, pwd, grp, valid));
}

void command_rmusr(char *command, int index) {
    char *usr = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "usr") == 0) {

                    if (stricmp(type, "usr") == 0) {
                        usr = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    remUser(newRmUsr(usr, valid));
}

void command_edit(char *command, int index) {
    int size = 0;
    char *path = "";
    char *cont = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;


        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "size") == 0 ||
                    stricmp(type, "cont") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "size") == 0) {
                        if (isNumber(arg)) {
                            size = atoi(arg);
                        } else {
                            alert(ERR, "Argumento no valido se esperaba UN ENTERO.");
                        }
                    }

                    if (stricmp(type, "cont") == 0) {
                        cont = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    edtFile(newEdit(path, size, cont, valid));
}

void command_cat(char *command, int index) {
    char *path[10][PATH_LENGHT];

    bool valid = TRUE;
    int in = 0;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                /*if(stricmp(type, "path") == 0 ||
                   stricmp(type, "name") == 0 )
                {

                  if(stricmp(type, "path") == 0){
                    path = arg;
                  }

                  if(stricmp(type, "name") == 0){
                    name = arg;
                  }
                }else{
                  alert(ERR, "no se reconoce el parametro especificado");
                  valid = FALSE;
                }*/
                strcpy((char *) path[in], arg);
                in++;

            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    catF(newCat(path));
}

void command_ren(char *command, int index) {
    char *path = "";
    char *name = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "name") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "name") == 0) {
                        name = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    renameF(newRen(path, name, valid));
}

void command_cp(char *command, int index) {
    char *path = "";
    char *dest = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "dest") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "dest") == 0) {
                        dest = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    cpFileFolder(newCp(path, dest, valid));
}

void command_mv(char *command, int index) {
    char *path = "";
    char *dest = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "dest") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "dest") == 0) {
                        dest = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    mvFileFolder(newMv(path, dest, valid));
}

void command_exec(char *command, int index) {
    char *path = "";

    bool valid = TRUE;
    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0) {
                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    if (valid == TRUE) {
        FILE *fp;
        char *line = NULL;
        size_t len = 0;
        ssize_t read;

        fp = fopen(path, "r");
        if (fp == NULL)
            exit(EXIT_FAILURE);
        int l = 1;
        while ((read = getline(&line, &len, fp)) != -1) {
            if (strcmp(line, "") != 0 && strcmp(line, "\n") != 0 && strcmp(line, "\r") != 0 &&
                strcmp(line, "\r\n") != 0 &&
                strcmp(line, "\n\r") != 0 && line[0] != '#') {

                size_t ln = strlen(line) - 1;

                if (line[ln] == '\n')
                    line[ln] = '\0';

                printf("line %d:\t", l);
                getCommand(line);
                printf("%s\n", "");

            }
            l++;
        }

        fclose(fp);
        if (line)
            free(line);
    }

}

void command_rep(char *command, int index) {
    char *id = "";
    char *path = "";
    char *name = "";
    char *ruta = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "path") == 0 ||
                    stricmp(type, "name") == 0 ||
                    stricmp(type, "ruta") == 0 ||
                    stricmp(type, "id") == 0) {

                    if (stricmp(type, "path") == 0) {
                        path = arg;
                    }

                    if (stricmp(type, "name") == 0) {
                        name = arg;
                    }

                    if (stricmp(type, "id") == 0) {
                        id = arg;
                    }

                    if (stricmp(type, "ruta") == 0) {
                        ruta = arg;
                    }

                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    genRep(newRep(name, path, id, ruta, valid));
}

void command_logout(char *command, int index) {
    logout();
}

void command_recover(char *command, int index) {
    char *id = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        //while(command[index]!= '-' && index < strlen(command)){
        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "id") == 0) {
                    if (stricmp(type, "id") == 0) {
                        id = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }
    printJournal(id);
}

void command_loss(char *command, int index) {
    char *id = "";
    bool valid = TRUE;

    while (index < strlen(command)) {
        if (command[index] == '-') {
            index++;
        } else if (command[index] == '#') {
            break;
        } else {
            alert(ERR, "se esperaba '-' ");
            valid = FALSE;
            break;
        }

        char *type = newString(8);
        char *arg = newString(100);
        int index_type = 0;
        int index_arg = 0;

        while (command[index] != '~' && index < strlen(command)) {
            type[index_type] = command[index];
            index++;
            index_type++;
        }
        index++;
        if (command[index] == ':') {
            index++;
            if (command[index] == '~') {
                index++;
                if (command[index] == '"') {
                    index++;
                    while (command[index] != '"' && index < strlen(command)) {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                    index++;
                } else {
                    while (command[index] != ' ' && index < strlen(command) && command[index] != '\r') {
                        arg[index_arg] = command[index];
                        index++;
                        index_arg++;
                    }
                }
                index++;

                if (stricmp(type, "id") == 0) {
                    if (stricmp(type, "id") == 0) {
                        id = arg;
                    }
                } else {
                    alert(ERR, "no se reconoce el parametro especificado");
                    valid = FALSE;
                }
            } else {
                alert(ERR, "se esperaba '~' ");
                valid = FALSE;
                break;
            }
        } else {
            alert(ERR, "se esperaba ':' ");
            valid = FALSE;
            break;
        }

    }

    clearBits(id);
}