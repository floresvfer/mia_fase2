#ifndef MKDIR_H
#define MKDIR_H

#include "utils.h"

struct mkdir {
    char *path;
    char *id;
    bool padre;
    bool valid;
} typedef mkDir;

mkDir *newMkDir(char *id, char *path, bool padre, bool valid);

#endif /* MKDIR_H */
