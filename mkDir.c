#include "mkDir.h"

mkDir *newMkDir(char *id, char *path, bool padre, bool valid) {
    mkDir *tmp = malloc(sizeof(mkDir));

    tmp->path = newString(PATH_LENGHT);
    tmp->valid = valid;
    tmp->padre = padre;
    tmp->id = id;

    tmp->path = path;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }
    return tmp;
}
