#ifndef UTILS_GPVZ_H_INCLUDED
#define UTILS_GPVZ_H_INCLUDED

#include <libgen.h>
#include <string.h>
#include <stdio.h>

#include "structures.h"
#include "utils.h"
#include "binaryFiles.h"

bool grapbSB(superBlock *sB, char *path);

bool graphMbr(Mbr *mbr, char *path);

bool graphDisk(Mbr *mbr, char *path);

bool graphFileContent(char *id, char *path, char *ruta);

double getScale(int sizeDisk, int sizePart);

int freeSpace(int a, int b);


void graphDiskContent(char *id, char *path);

void inodeString(FILE *destino, char *id, int index);

void blockString(FILE *destino, char *id, int index, char type, bool first);


void graphInodes(char *id, char *path);

void inodeListString(FILE *destino, char *id, int index);


#endif // UTILS_GPVZ_H_INCLUDED
