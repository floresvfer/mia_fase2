#ifndef MIA_CP_H
#define MIA_CP_H

#include "structures.h"
#include "utils.h"

typedef struct cp {
    char *path1[PATH_LENGHT];
    char *path2[PATH_LENGHT];
    bool valid;
} Cp;

Cp *newCp(char *path1, char *path2, bool valid);

#endif //MIA_CP_H
