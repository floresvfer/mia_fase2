#ifndef REN_H
#define REN_H

#include "utils.h"

struct ren {
    char *path;
    char *name;
    bool valid;
} typedef Ren;

Ren *newRen(char *path, char *name, bool valid);

#endif /* REN_H */
