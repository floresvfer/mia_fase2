#include "ren.h"

Ren *newRen(char *path, char *name, bool valid) {
    Ren *tmp = malloc(sizeof(Ren));

    tmp->path = newString(PATH_LENGHT);
    tmp->name = newString(12);
    tmp->valid = valid;

    tmp->path = path;
    tmp->name = name;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }

    if (strlen(name) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -name OBLIGATORIO");
    }
    return tmp;
}
