#include "chmod.h"

Chmod *newChmod(char *path, int ugo, bool recursivo, bool valid) {
    Chmod *tmp = malloc(sizeof(Chmod));
    tmp->path = newString(PATH_LENGHT);
    tmp->valid = valid;
    tmp->ugo = ugo;
    tmp->recursivo = recursivo;
    tmp->path = path;

    if (strlen(path) == 0) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -path OBLIGATORIO");
    }
    if (ugo == -1) {
        tmp->valid = FALSE;
        alert(WAR, "parametro -ugo OBLIGATORIO");
    }
    return tmp;
}

