#ifndef BINARYFILES_H_INCLUDED
#define BINARYFILES_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <string.h>

#include "utils.h"
#include "utils_gpvz.h"

#include "commands.h"
#include "structures.h"
#include "mkDisk.h"
#include "rmDisk.h"
#include "fDisk.h"
#include "mount.h"
#include "unMount.h"

#include "mkFs.h"
#include "mkFile.h"
#include "rem.h"
#include "chmod.h"
#include "chown.h"
#include "mkGrp.h"
#include "mkUsr.h"
#include "rmUsr.h"
#include "edit.h"
#include "cat.h"
#include "ren.h"
#include "Cp.h"
#include "mv.h"
#include "mkDir.h"
#include "recover.h"

#include "logIn.h"

#include "rep.h"
#include "list.h"

list *mounts;
list_n *nodes;
int idUser;
int idGrupo;
char *diskName;
char ids;


bool newPartition(fDisk *data);

int getEspacioLibre(int ps1, int psz1, int ps2);

void ingresar(Login *data);

void newDisk(mkDisk *data);

void delDisk(rmDisk *data);

void genRep(Rep *data);

void mountPartition(mount *data);

void unMountPartition(unMount *data);

void mkFsPartition(mkFs *data);

void clearBits(char *id);

void makeFile(mkFile *data);

void removeFile_Folder(Rem *data);

void chMod(Chmod *data);

void chOwn(Chown *data);

void makeGroup(mkGrp *data);

void remGroup(mkGrp *data);

void makeUser(mkUsr *data);

void remUser(rmUsr *data);

void edtFile(Edit *data);

void renameF(Ren *data);

void cpFileFolder(Cp *data);

void mvFileFolder(Mv *data);

void catF(Cat *data);

void makeFolder(mkDir *data);


void initializeList();

int getPartStart(char *id);

int getPartSize(char *id);

char *getPartName(char *id);

bool getSuperBlock(superBlock *sB, char *id);

bool getInode(inode *In, char *id, int index);

bool getFileBlock(fBlock *fB, char *id, int index);

bool getFolderBlock(dBlock *dB, char *id, int index);

bool getPointerBlock(pBlock *pB, char *id, int index);


int copyFileFolder(char *id, char *pathF, char *pathD);

void folder(char *id, int index, char *currentFolderF, char *currentFolderD);

void block(char *id, int index, char type, bool first, char *currentFolderF, char *currentFolderD);


int getFolder(char *id, char *path, bool father);

int getFileFolder(char *id, char *path);

int changePermisos(char *id, char *path, int perms, bool recursivo);

int changePropietario(char *id, char *path, int usr, int grp, bool recursivo);

void getFileFolderName(char *destiny, char *path);

int getLastSlash(char *path);

int renameFileFolder(char *id, int inodeFolder, char *prevName, char *newName);

int editFile(char *id, int inodeFolder, char *name, char *content);

int removeFile(char *id, int inodeFolder, char *name);

int removeFolder(char *id, int inodeFolder);

int removeFileFolderName(char *id, int blockIndex, int index);

int getFileContent(char *id, int inodeFolder, char *name, char *destino);

int writeFolder(char *id, int inodeFolder, char *name);

int writeFile(char *id, int inodeFolder, char *name, char *content);


void writeInode(char *id, int index, int i_uid, int i_gid, int i_size, char i_type, int i_perm);

void writeFileBlock(char *id, int index, char *content);

void writeFolderBlock(char *id, int index);

void writeFirstFolderBlock(char *id, int index, int padre, int me);

void writePointerBlock(char *id, int index);

void writeBlockBitMap(char *id);

void writeInodeBitMap(char *id);

void writeJournal(char *id);

void addJournal(char *id, int typo_OP,
                int typo,
                char *cont,
                char *path1,
                char *path2,
                int propietario,
                int permisos);


void printJournal(char *id);

int firstFreeInode(char *id);

int firstFreeBlock(char *id);

void writeInodeBit(char *id, int index, bool status);

void writeBlockBit(char *id, int index, bool status);

void updateInodeAD(char *id, int index, int AD, int value);

void updateFolderBlock(dBlock *dB, char *id, int index);


char getInodeBit(char *id, int index);

char getBlockBit(char *id, int index);


void printBlockBitMap(char *id, int perLine, char deliMeter, char *path);

void printInodeBitMap(char *id, int perLine, char deliMeter, char *path);

void printSuperBlock(char *id);

void printInode(char *id, int index);

void printBlock(char *id, int index, char type);


int getGroups(char *file, char *destino);

int getAllUsers(char *file, char *destino);

int getUsers(char *file, char *g, char *destino);

int findGroupID(char *file, char *g);

int findUserID(char *users, char *usr);

void findUserGroup(char *users, char *usr, char *destino);

void findUserPsswd(char *users, char *usr, char *destino);

int nextUserID(char *file);

int nextGroupID(char *file);

int addGroup(char *file, char *name, char *newFile); // 'newFile' => el nuevo string a escribir en archivo
int removeGroup(char *file, char *n, char *newFile);

int addUser(char *file, char *group, char *name, char *psswd, char *newFile);

int removeUser(char *file, char *n, char *newFile);

void logout();

void logIn(char *file, char *user, char *psswd);

#endif // BINARYFILES_H_INCLUDED
